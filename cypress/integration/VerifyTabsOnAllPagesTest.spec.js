import HomeTabsAction from "../Action/TabsAction.spec";
import HomeAction from "../Action/HomeAction.js";
import SettingAction from "../Action/SettingAction.js";
import BaseAction from "../Action/BaseAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import CyclesAction from "../Action/CyclesAction";
import UserAction from "../Action/UserAction.js";
import TrainingAction from "../Action/TrainingAction";
import ModulesAction from "../Action/ModulesAction";
import PathAction from "../Action/PathAction";
import SurveysAction from "../Action/SurveysAction";
import CertificatesAction from "../Action/CertificatesAction";
import moment from "moment";

describe('Tab verification', () => {


    const homeaction = new HomeAction;
    const settingaction = new SettingAction;
    const TabsAction = new HomeTabsAction;
    const baseaction = new BaseAction;
    const questionnaireaction = new QuestionnaireAction;
    const cyclesaction = new CyclesAction();
    const trainingaction = new TrainingAction();
    const useraction = new UserAction;
    const modulesaction = new ModulesAction();
    const pathsaction = new PathAction();
    const surveysaction = new SurveysAction();
    const certificatesaction = new CertificatesAction();

    let admindatas;
    let link;
    let basedatas;
    let settingdata;
    let questiondatas;
    let questiondatas2;
    let questionnairedatas;
    let cycledata;
    let trainingdata;
    let usersdatas;
    let without_mail_user;
    let modulesdata;
    let pathdata;
    let surveydata;

    beforeEach(() => {
        cy.visit('/')
        cy.viewport(1280, 720);

        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
        })

        cy.fixture("base_questionnaires").then((data) => {
            basedatas = data.bases.base_1;
            questionnairedatas = data.questionnaires.questionnaire_1;
            cycledata = data.cycle;
            trainingdata = data.training.training_1;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
        })

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
            questiondatas2 = data.single_type;
        })

        cy.fixture('media_modules').then((data)=>{
            modulesdata = data.module.module_list;
            pathdata = data.path.path1;
        })

        cy.fixture('form').then((data)=>{
            surveydata = data.forms.simple_form;
        })

    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it('1. Tab verification',()=>{
        TabsAction.verify_all_tabs_at_home_page();
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        TabsAction.verify_all_tabs_at_dashboard_page();
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        TabsAction.verify_all_tabs_at_base_page(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas2);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas);
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas);
        TabsAction.verify_all_tabs_at_questionnaire_page(basedatas,questionnairedatas);
        useraction.create_one_user_by_admin(without_mail_user)
        useraction.activate_the_created_user_by_admin_without_logout(without_mail_user)
        cyclesaction.create_cycles(cycledata);
        cyclesaction.create_steps_into_created_cycles(basedatas,cycledata,questionnairedatas);
        TabsAction.verify_all_tabs_at_cycles_page(cycledata);
        trainingaction.create_trainings_with_cycle(basedatas,trainingdata,admindatas,cycledata);
        TabsAction.verify_trainings_tab_before_add_participants(trainingdata,basedatas);
        trainingaction.add_participants_into_created_training(basedatas,without_mail_user,trainingdata);
        TabsAction.verify_trainings_tab_after_added_participants(trainingdata,basedatas);
        TabsAction.verify_all_tabs_at_media_page();
        TabsAction.verify_all_tabs_at_users_page(admindatas);
        TabsAction.verify_all_tabs_at_result_page();
        TabsAction.verify_all_tabs_at_skills_page();
        modulesaction.create_module(modulesdata);
        modulesaction.add_element_into_created_module(basedatas,questionnairedatas,modulesdata);
        TabsAction.verify_all_tabs_at_modules_page(modulesdata);
        pathsaction.create_path(pathdata);
        pathsaction.add_module_into_created_path(pathdata,modulesdata,basedatas);
        TabsAction.verify_all_tabs_at_path_page(pathdata);
        surveysaction.create_surveys(surveydata);
        surveysaction.create_all_fields(surveydata);
        TabsAction.verify_all_tabs_at_surveys_page(surveydata);
        certificatesaction.open_certificates_module();
        TabsAction.verify_all_tabs_at_certificates_page();
        TabsAction.verify_all_tabs_at_settings_page();
        TabsAction.verify_all_tabs_at_business_intelligence();
        settingaction.enable_all_user_type_option_and_their_side_menu()
        TabsAction.open_user_view();
        TabsAction.verify_all_tabs_at_users_my_results();
        TabsAction.verify_all_tabs_at_users_my_certificates();
        TabsAction.verify_all_tabs_at_users_my_skills();
        TabsAction.open_admin_view();
        homeaction.delete_admin_account_if_signed(link)


    })

})