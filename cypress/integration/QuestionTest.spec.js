import HomeAction from "../Action/HomeAction.js";
import BaseAction from "../Action/BaseAction.js";
import UserAction from "../Action/UserAction.js";
import MediaAction from "../Action/MediaAction.js";
import SettingAction from "../Action/SettingAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import QuestionAction from "../Action/QuestionAction";

describe('Question', () => {

    const homeaction = new HomeAction;
    const baseaction = new BaseAction;
    const useraction = new UserAction;
    const mediaaction = new MediaAction;
    const settingaction = new SettingAction;
    const questionnaireaction = new QuestionnaireAction;
    const questionaction = new QuestionAction()


    let basedatas1;
    let basedatas2;
    let questiondatas1;
    let questiondatas2;
    let correction_que;
    let questionnairedatas;
    let admindatas;
    let usersdatas;
    let without_mail_user;
    let link;
    let settingdata;
    let mediadata;

    beforeEach(() => {

        cy.viewport(1280, 720);

        cy.exec('npm cache clear --force')


        cy.fixture("base_questionnaires").then((data) => {
            basedatas1 = data.bases.base_0;
            basedatas2 = data.bases.base_1;
            questionnairedatas = data.questionnaires.questionnaire_1;
        })

        cy.fixture("questions").then((data) => {
            questiondatas1 = data.all_type;
            correction_que = data.all_type_with_correction_question;
            questiondatas2 = data.single_type;
        })

        cy.fixture("users").then((data) => {
            // admindatas = data.tets_beta_admin;
            admindatas = data.tets_www_admin;
            // admindatas = data.priyanga;
            usersdatas = data.users.tets2020;
            without_mail_user = data.users.Inactive_mail_user;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
        })

        cy.fixture("media_modules").then((data) => {
            mediadata = data.medias.media1.items;
        })
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })


    it('1. Create all type of questions without free and edit each created question on the question page', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_without_domain_and_verify(basedatas1);
        baseaction.create_question_on_created_base(basedatas1, questiondatas1)
        questionaction.edit_create_question_on_question_list_page(basedatas1, questiondatas1)
        homeaction.delete_admin_account_if_signed(link)
    })


    it('2. Create all type of questions without free and play each created question on the question page', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas)
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_without_domain_and_verify(basedatas1)
        baseaction.create_question_on_created_base(basedatas1, questiondatas2)
        questionaction.play_each_create_question_on_question_list_page(basedatas1, questiondatas2)
        homeaction.delete_admin_account_if_signed(link)
    })

    it('3. Create all type with free form answer question and play each created question on the question page', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas2);
        baseaction.create_all_question_with_free_on_created_base(basedatas2, correction_que)
        questionaction.play_each_create_question_on_question_list_page_with_free(basedatas2, correction_que)
        homeaction.delete_admin_account_if_signed(link)

    })

    it('4. Create all type of questions and duplicate each created ' +
        'question on the question page and verify that the duplicate question' +
        'and delete the original question and verify', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_without_domain_and_verify(basedatas1);
        baseaction.create_question_on_created_base(basedatas1, questiondatas1)
        questionaction.create_duplicate_on_each_create_question_on_question_list_page(basedatas1, questiondatas1)
        homeaction.delete_admin_account_if_signed(link)
    })

    it('5. Export a particular base question to a DOCX file', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_without_domain_and_verify(basedatas1);
        baseaction.create_base_with_single_domain_and_verify(basedatas2);
        baseaction.create_question_on_created_base(basedatas1, questiondatas2)
        baseaction.create_question_on_created_base(basedatas2, questiondatas2)
        questionaction.export_particular_base_question_to_docx_file(basedatas2)
        homeaction.delete_admin_account_if_signed(link)
    })

    it('6. Upload a sample question file on a created base and verify', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_without_domain_and_verify(basedatas1);
        baseaction.upload_sample_question_file_through_base(basedatas1)
        homeaction.delete_admin_account_if_signed(link)

    })

    it('7. Create a question with same rank and verify its alert message', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_without_domain_and_verify(basedatas1);
        baseaction.create_question_with_same_rank_on_created_base(basedatas1, questiondatas2)
        homeaction.delete_admin_account_if_signed(link)

    })

    it('8. Copy a question to another base', () => {
        cy.visit('/')
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_without_domain_and_verify(basedatas1);
        baseaction.create_base_with_single_domain_and_verify(basedatas2);
        baseaction.create_question_on_created_base(basedatas1, questiondatas2)
        questionaction.copy_created_question_and_move_to_another_base(basedatas1,basedatas2, questiondatas2)
        questionaction.verify_copied_question_is_on_bases(basedatas2, questiondatas2)
        homeaction.delete_admin_account_if_signed(link)
    })





})
