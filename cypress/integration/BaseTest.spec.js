import HomeAction from "../Action/HomeAction.js";
import BaseAction from "../Action/BaseAction.js";
import UserAction from "../Action/UserAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import BaseModuleAction from "../Action/BaseModuleAction.js"
import dayjs from "dayjs";

describe('Bases', () => {

    const homeaction = new HomeAction;
    const baseaction = new BaseAction;
    const useraction = new UserAction;
    const questionnaireaction = new QuestionnaireAction;
    const baseModuleaction = new BaseModuleAction();
    let pattern = "YYYYMMDDHHmmss";

    let basedatas;
    let basedatas1;
    let questiondatas;
    let questionnairedatas;
    let admindatas;
    let link;
    let mediadata;
    let without_mail_user;
    let simple_surveydata;
    let modulesdata;
    let training;
    let randomNumber = 0;

    beforeEach(() => {
        cy.exec('npm cache clear --force');
        cy.viewport(1280, 720);
        cy.fixture("base_questionnaires").then((data) => {
            basedatas = data.bases.tagBase_1;
            basedatas1 = data.bases.tagBase_2;
            training = data.training.training_1;
        });

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
        });

        cy.fixture("base_questionnaires").then((data) => {
            questionnairedatas = data.questionnaires.questionnaire_1;
        });

        cy.fixture("users").then((data) => {
            admindatas = data.automationAccount;
            without_mail_user = data.users.Inactive_mail_user;
        });

        cy.fixture("common_data").then((data) => {
            link = data.links;
        });

        cy.fixture("media_modules").then((data) => {
            mediadata = data.medias.media1.items;
            modulesdata = data.module.module_list;
        });

        cy.fixture('form').then((data) => {
            simple_surveydata = data.forms.simple_form;
        });

        cy.writeFile('cypress/fixtures/user_reg_link.json', '{"user_reg_link":""}')

    });

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    });

    it('Creating base with questions to create dublicate base', () => {
        randomNumber = dayjs().format(pattern);
        cy.visit('/');
        homeaction.admin_user_sign_in(admindatas);
        baseaction.create_base_with_single_domain_and_verify("Duplicate Base "+randomNumber,"dupl","dup","Creating duplicate base",["python"]);
        baseaction.upload_sample_question_file_through_base("Duplicate Base "+randomNumber);
        baseModuleaction.EditQuestionsToChangeStatus("Duplicate Base "+randomNumber, questiondatas);
        baseModuleaction.createQuestionnaireWithDifferentMode("Duplicate Base "+randomNumber, simple_surveydata, randomNumber);
        baseaction.create_media_on_base("Duplicate Base "+randomNumber,mediadata,randomNumber);
        baseModuleaction.openOverviewTab("Duplicate Base "+randomNumber);
        baseModuleaction.getQuestionsCount();
        baseModuleaction.getQuestionnaireCount();
        baseModuleaction.createDuplicateBase("Duplicate Base "+randomNumber);
        baseModuleaction.verifyDuplicateBaseCreatedAsExpected("Duplicate Base "+randomNumber,mediadata)
    });

    it('Transfer the created base to same enterprise and verify that it trasfered', () => {
        randomNumber = dayjs().format(pattern);
        cy.visit('/');
        homeaction.admin_user_sign_in(admindatas);
        baseaction.create_base_with_single_domain_and_verify("Transfer Base "+randomNumber,"trans","tra","Transfer base within enterprise",["python"]);
        baseaction.upload_sample_question_file_through_base("Transfer Base "+randomNumber);
        baseModuleaction.EditQuestionsToChangeStatus("Transfer Base "+randomNumber, questiondatas);
        baseaction.create_media_on_base("Transfer Base "+randomNumber,mediadata,randomNumber);
        baseModuleaction.createQuestionnaireWithDifferentMode("Transfer Base "+randomNumber, simple_surveydata, randomNumber);
        baseModuleaction.openOverviewTab("Transfer Base "+randomNumber);
        baseModuleaction.getQuestionsCount();
        baseModuleaction.getQuestionnaireCount();
        baseModuleaction.TransferBase("Transfer Base "+randomNumber, randomNumber);
        baseModuleaction.CopyLinkOfTranferBase(randomNumber);
        baseModuleaction.transferByLinkAndVerify("Transfer Base "+randomNumber);
        baseModuleaction.verifyTransferBaseCreatedAsExpected("Transfer Base "+randomNumber,mediadata)
    });

    it('Create Base with Tag', () => {
        cy.visit('/');
        randomNumber = dayjs().format(pattern);
        homeaction.admin_user_sign_in(admindatas);
        baseaction.createBaseWithTag(basedatas, basedatas1,randomNumber);
        baseaction.upload_sample_question_file_through_base(basedatas['base_name']+" "+randomNumber);
        questionnaireaction.create_static_type_questionnaire(basedatas['base_name']+" "+randomNumber,questionnairedatas,randomNumber);
        baseaction.create_trainings_without_cycle(basedatas['base_name']+" "+randomNumber, training, admindatas,randomNumber);
        baseaction.create_media_on_base(basedatas['base_name']+" "+randomNumber,mediadata,randomNumber);
        baseaction.verifyToFilterwithTag(basedatas['base_name']+" "+randomNumber,basedatas,randomNumber);
        baseaction.verifyTheArchivedBaseInQuestionnaire(basedatas['base_name']+" "+randomNumber, questionnairedatas);
        baseaction.verifyTheArchivedBaseInQuestions(basedatas['base_name']+" "+randomNumber, questiondatas);
        baseaction.verifyTheArchivedBaseInMedia(basedatas['base_name']+" "+randomNumber, mediadata,randomNumber);
        baseaction.verifyTheArchivedBaseInTraining(basedatas['base_name']+" "+randomNumber, training);
        baseaction.verifyTheActiveFilterInBase(basedatas['base_name']+" "+randomNumber)
    });

    it.only('Edit The Existing Base', () => {
        cy.visit('/');
        randomNumber = 
        dayjs().format(pattern);
        homeaction.admin_user_sign_in(admindatas);
        baseaction.create_base_with_single_domain_and_verify("Editing Base "+randomNumber,"Edi","Edi","Editing base",["python"]);
        baseaction.verifyTheCreatedBaseDetails("Editing Base "+randomNumber,["python"],admindatas);
        useraction.create_one_user_by_admin(without_mail_user,randomNumber);
        useraction.activate_the_created_user_by_admin_without_logout(without_mail_user, randomNumber);
        baseaction.editBaseDetails("Editing Base "+randomNumber,"Edited Base "+randomNumber,["Ruby"], without_mail_user,randomNumber);
        baseaction.verifyEditedBaseDetails("Edited Base "+randomNumber, ["Ruby"],without_mail_user,randomNumber)
    });

    it('Delete the Base', () => {
        cy.visit('/');
        randomNumber = dayjs().format(pattern);
        homeaction.admin_user_sign_in(admindatas);
        baseaction.create_base_with_single_domain_and_verify("Delete Base "+randomNumber,"del","Del","Delete base",["python"]);
        baseaction.upload_sample_question_file_through_base("Delete Base "+randomNumber);
        questionnaireaction.create_static_type_questionnaire("Delete Base "+randomNumber,questionnairedatas,randomNumber);
        baseaction.create_trainings_without_cycle("Delete Base "+randomNumber, training, admindatas,randomNumber);
        baseaction.create_media_on_base("Delete Base "+randomNumber,mediadata,randomNumber);
        baseaction.create_module("Delete Base "+randomNumber, modulesdata,randomNumber);
        // baseaction.add_element_into_created_module("Delete Base "+randomNumber,modulesdata,randomNumber);
        baseaction.deleteCreatedBase("Delete Base "+randomNumber);
        baseaction.verifyDeletedBaseInQuestionnaire("Delete Base "+randomNumber, questionnairedatas,randomNumber);
        baseaction.verifyDeletedBaseInQuestions("Delete Base "+randomNumber, questiondatas);
        baseaction.verifyDeletedBaseInMedia("Delete Base "+randomNumber, mediadata);
        baseaction.verifyDeletedBaseInTraining("Delete Base "+randomNumber, training);
        baseaction.verifyDeletedBaseInModules("Delete Base "+randomNumber,randomNumber)
    })

});





