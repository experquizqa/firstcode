import HomeAction from "../Action/HomeAction.js";
import SettingAction from "../Action/SettingAction.js";
import BaseAction from "../Action/BaseAction.js";
import QuestionnaireAction from "../Action/QuestionnaireAction.js";
import ModulesAction from "../Action/ModulesAction";
import PathAction from "../Action/PathAction";

describe('Paths',()=>{

    const homeaction = new HomeAction();
    const settingaction = new SettingAction();
    const baseaction = new BaseAction();
    const questionnaireaction = new QuestionnaireAction();
    const modulesaction = new ModulesAction();
    const pathsaction = new PathAction();

    let admindatas;
    let link;
    let settingdata;
    let basedatas;
    let questiondatas;
    let questiondatas2;
    let questionnairedatas;
    let modulesdata;
    let pathdata;

    beforeEach(()=>{
        cy.visit('/')
        cy.viewport(1280, 720);

        cy.fixture('base_questionnaires').then((data)=>{
            basedatas = data.bases.base_1;
            questionnairedatas = data.questionnaires.questionnaire_1;
        })

        cy.fixture("users").then((data) => {
            admindatas = data.tets_beta_admin;
            // admindatas = data.tets_www_admin;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data
        })

        cy.fixture("questions").then((data) => {
            questiondatas = data.all_type;
            questiondatas2 = data.single_type;
        })

        cy.fixture('media_modules').then((data)=>{
            modulesdata = data.module.module_list;
            pathdata = data.path.path1;
        })
    })

    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })


    it('1. Create path',()=>{
        homeaction.verify_admin_exist_and_deleted(admindatas, link);
        homeaction.register_as_administraion(admindatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(admindatas, settingdata, link)
        baseaction.create_base_with_single_domain_and_verify(basedatas);
        baseaction.create_question_on_created_base(basedatas, questiondatas);
        questionnaireaction.create_static_type_questionnaire(basedatas, questionnairedatas);
        questionnaireaction.select_all_question_on_created_static_type_questionnaire(basedatas, questionnairedatas);
        modulesaction.create_module(modulesdata);
        modulesaction.add_element_into_created_module(basedatas,questionnairedatas,modulesdata);
        pathsaction.create_path(pathdata);
        pathsaction.add_module_into_created_path(pathdata,modulesdata);
        homeaction.delete_admin_account_if_signed(link)
    })


})