import HomeAction from "../Action/HomeAction.js";
import ReadMail from "../Mail/ReadMail";
import SettingAction from "../Action/SettingAction.js";

describe('Home', function () {
    const homeaction = new HomeAction();
    const readmail = new ReadMail()
    const settingaction = new SettingAction;

    let AdminDatas;
    let link;
    let settingdata;


    beforeEach(() => {
        cy.viewport(1280, 720);

        cy.exec('npm cache clear --force')

        cy.fixture("users").then((data) => {
            AdminDatas = data.automationAccount;
        })

        cy.fixture("common_data").then((data) => {
            link = data.links;
            settingdata = data;
        })
    })


    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })

    it("1.Register as administration", function () {
        cy.visit('/')
        homeaction.register_as_administraion(AdminDatas);
        homeaction.verifyAdminAccountCreated(AdminDatas);
        settingaction.enable_all_settings_option_and_verify_side_menu(AdminDatas, settingdata, link)
        settingaction.enable_all_user_type_option_and_their_side_menu()
    })
    it.only("2. Register Without Mandatory Field", function () {
        cy.visit('/')
        homeaction.registerWithoutMandatoryField(requirefieldtest);

    })
    it("3. RegisterWithExistEmailField", function () {
        cy.visit('/')

        homeaction.registerWithExistEmailField(requirefieldtest);
    }
    )
    it("4. RegisterWithUnlimitedCharactersOnAllField", function () {
        cy.visit('/')
        homeaction.registerWithoutLimitedchracteronallField(characterlimit);
    }
    )
    it("5. registerWithinvalidConfirmPassword", function () {
        cy.visit('/')
        homeaction.registerWithinvalidConfirmPassword(requirefieldtest, invalidConfirmpassword);
        

    }
    )
    it("6. registerFromsigninPopup", function () {
        randomNumber = dayjs().format(pattern);
        cy.visit('/')
        homeaction.registerFromsighinPopup();
        homeaction.registerfromSignInpopup(DuplicateDatas, randomNumber);
        homeaction.verifyAdminAccountCreated(DuplicateDatas);
    }
    )
})

