﻿/// <reference types="cypress" />
// const path = require("path");
// const gmail_testers = require("gmail-tester");
// const xlsx = require("node-xlsx").default;
// const fs = require('fs');
// const { dirname } = require("path");
// const google = require('googleapis');
// const gmail = google.gmail_v1;
// const gmail = google.gmail('v1')
// const {OAuth2Client} = require('google-auth-library');
// const {promisify} = require('util');
// const readFileAsync = promisify(fs.readFile);




/// <reference types="cypress" />

const path = require("path");
const gmail_testers = require("gmail-tester");
const xlsx = require("node-xlsx").default;
const fs = require('fs');
const { dirname } = require("path");
const {google} = require("googleapis");
const nodemailer = require("nodemailer");
const smtpTransport = require("nodemailer-smtp-transport");
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
    on('task', {
        "gmailchecks": async args => {
            const email = await gmail_testers.check_inbox(
                // path.resolve(--dirname, "credentials.json"),
                // path.resolve(--dirname, "gmail_token.json"),
                "./credentials.json",
                "./gmail_token.json",
                args.options
            );
            return email;
        }
    });
    on("task", {
        "gmailcheck": async args => {
            const messages = await gmail_testers.get_messages(
                // path.resolve(--dirname, "credentials.json"),
                // path.resolve(--dirname, "gmail_token.json"),
                "./credentials.json",
                "./gmail_token.json",
                args.options
            );
            return messages;
        }
    });

    on('task', {
        setCopiedLink: (val) => {
          return (href = val)
        },
        getCopiedLink: () => {
          return href
        },
      })
}

