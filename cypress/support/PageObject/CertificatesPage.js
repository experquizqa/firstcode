class CertificatesPage {
//To click the certificate menu from side menu
    click_certificates_side_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Certificates")
        .click()           
    }
}

export default CertificatesPage