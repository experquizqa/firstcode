class AdminDashboardPage {

    click_dashaboard_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Dashboard").click();
    }

    click_admin_base_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Bases").click()
    }

    click_admin_question_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Questions").click()
    }

    click_admin_questionnaire_menu() {
        cy.wait(5000)
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Questionnaires")
            .click()
    }

    log_out_admin() {
        cy.xpath("//div[@class='login-slide']")
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']",{ timeout: 10000 }).should("be.visible")
            .click()
    }

    click_my_profile_option_on_admin_profile() {
        cy.xpath("//div[@class='login-slide']")
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']",{ timeout: 10000 }).should("be.visible")
            .click()
    }

}

export default AdminDashboardPage;
