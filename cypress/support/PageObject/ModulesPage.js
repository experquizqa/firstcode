import questionnairePage from "./QuestionnairePage";
import QuestionnairePage from "./QuestionnairePage";
import moment from "moment";

const questionnairepage = new QuestionnairePage();

class ModulesPage {

//To click the modules menu from side menu
    click_modules_side_menu() {
        cy.xpath("//div[@class='nav-wrapper']//a[@href='/enterprise/modules/list']//span[text()='Modules']/../i",{timeout:50000}).click({force:true})
    }

    //To click the new modules button at modules page
    click_new_modules_button() {
        cy.xpath("//a[@id='new-module-btn'][normalize-space(text()='New Modules')]",{timeout:50000}).should("be.visible").click()
    }

    //To enter module name at modules creation page
    enter_module_name(module_name) {
        cy.xpath("//div[@class='bloc-body']//div//div//label[contains(text(),'Name of module')]//..//input[@id='module_name']",{timeout:50000})
            .should("be.visible").type(module_name)
    }

    //To click the save button at the module creation page
    click_save_button_at_module_creation_page() {
        cy.xpath("//button[@type='submit'][@form='module-form']",{timeout:50000}).should("be.visible").click()
    }

    //To open the created module
    open_created_module(module) {
        cy.xpath("//a[contains(@class,'cell module-name')][contains(normalize-space(),'" + module + "')]",{timeout:50000})
            .should("be.visible").click()
    }

    //To click the elements tab at module overview page
    click_elements_tab() {
        cy.xpath("//a[text()='Elements']",{timeout:50000}).should("be.visible").click()
    }

    click_steps_tab_on_module() {
        cy.xpath("//a[text()='Steps']",{timeout:50000}).should("be.visible").click()
    }

    //To filter created base on element page of modules
    filter_created_base_on_element_page(basedatas,type) {
        if(type === "questionnaire"){
            cy.xpath("//div[@id='select-questionnaire-tab']//span[@id='topic-filter-button']",{timeout:50000})
                .should("be.visible").click();
            this.select_base_on_topic_filter_field(basedatas)
            }else{
            cy.xpath("//div[@id='select-media-tab']//span[@id='media-topic-filter-button']",{timeout:50000})
                .should("be.visible").click();
            this.select_base_on_topic_filter_field_in_media(basedatas)
        }

    }


    //To select all options
    select_all_topic_option() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All active topics')]",{timeout:50000}).should("be.visible").click();
    }

    select_base_on_topic_filter_field(basedatas) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + basedatas + "']",{timeout:50000}).should("be.visible").click();
    }

    //To select the base on the topic filter
    select_base_on_topic_filter_field_in_media(basedatas) {
        cy.xpath("//ul[@id='media-topic-filter-menu']//div[text()='" + basedatas + "']",{timeout:50000}).should("be.visible").click();
    }

    //To add the questionnaire to the module
    select_questionnaire_to_module(ele) {
        cy.xpath("//span[contains(text(),'" + ele["name"] + "')]",{timeout:50000}).should("be.visible")
            .trigger("mousedown", {which: 1,force:true})
        cy.get('#items-set-3',{timeout:50000}).should("be.visible")
            .trigger("mousemove",)
            .trigger("mouseup", )
    }

    select_image_on_module_steps_page(ele,randomNumber) {
        cy.xpath("//span[contains(text(),'" + ele["name"] + "')]/../span[contains(text(),'" + randomNumber + "')]",{timeout:50000}).should("be.visible")
            .trigger("mousedown", {which: 1})
        cy.get('#items-set-3',{timeout:50000}).should("be.visible")
            .trigger("mousemove")
            .trigger("mouseup", )
    }

    //To verify success message after added elements into the modules
    verify_success_message_after_added_element_into_module() {
        cy.xpath("//div[@class='xq-flashes xq-m collapse']//ul//li[normalize-space(text()='Your settings have been saved')]", {timeout: 30000})
            .should('be.visible');
        cy.wait(5000)
    }

    click_module_trial_play_button(module) {
        cy.xpath("//a[contains(@class,'cell module-name')][contains(normalize-space(.),'" + module["module_name"] + "')]/..//a[@title='Try module']",{timeout:50000})
            .should("be.visible").click()
    }

    play_module(module, question) {
        if (module.type === "questionnaire") {
            this.click_to_play_questionnaire_on_module_steps_player_page(module)
            this.answer_static_questionnaire_on_module_player_page(question)
        }
    }

    click_to_play_questionnaire_on_module_steps_player_page(module) {
        cy.xpath("//div[@class='new-play-card-wrapper']//div[contains(text(),'" + module["name"] + "')]",{timeout:50000}).should("be.visible").click()
    }

    answer_static_questionnaire_on_module_player_page(question) {
        questionnairepage.verify_start_button_before_playing_static_questionnaire()
        questionnairepage.select_answers_in_static_player_by_active_user_on_module_test(question)
    }

    click_next_step_button_on_module_player_page() {
        cy.xpath("//div[@id='std-toolbar']//a[@id='next-btn']",{timeout:50000}).should("be.visible").click()
    }

    close_module_test() {
        cy.xpath("//div[@id='std-toolbar']/a[@data-target='#module-close-modal']",{timeout:50000}).should("be.visible").click()
    }

    click_close_button_on_confirmation_popup() {
        cy.xpath("//*[@id='save-btn']",{timeout:50000}).should("be.visible").click()
    }

    click_select_questionnaire_tab_on_module_elements_page() {
        cy.xpath("//a[text()='Questionnaires']",{timeout:50000}).should("be.visible").click({force:true})
    }

    click_select_media_tab_on_module_elements_page() {
        cy.xpath("//a[text()='Medias']",{timeout:50000}).should("be.visible").click({force:true})
    }

    enable_practice_usage_on_module_steps_page() {
        cy.xpath("//label[@for='elements_options-0-usage-0']",{timeout:50000}).should("be.visible").click()
    }

    enable_evaluation_usage_on_module_steps_page() {
        cy.xpath("//label[@for='elements_options-1-usage-1']",{timeout:50000}).should("be.visible").click()
    }

    enable_certification_usage_on_module_steps_page() {
        cy.xpath("//label[@for='elements_options-2-usage-2']",{timeout:50000}).should("be.visible").click()
    }

    click_save_button_on_module_steps_page() {
        cy.xpath("//button[text()='Save']",{timeout:50000}).should("be.visible").click()
    }

    click_invitation_tab_on_module() {
        cy.xpath("//a[text()='Invitation']",{timeout:50000}).should("be.visible").click()
    }

    enter_mail_subject(mail_sub, rand) {
        cy.xpath("//input[@id='email_subject']",{timeout:50000}).should("be.visible")
            .clear()
            .type(mail_sub["mail_subject"] + rand["rand_no"])
    }

    enter_mail_message() {
        cy.xpath("//div[@class='email-message ']//textarea[@id='email_message']",{timeout:50000}).should("be.visible")
            .clear()
            .type(mail_sub["mail_message"])
    }

    current_date() {
        let current_date_with_time
        const today_date = moment()

        const tdy_date_format = today_date.format('YYYY-MM-DD')
        const nowTime = today_date.format('H:m')
        current_date_with_time = tdy_date_format + " " + nowTime;
        return current_date_with_time
    }


    tomorrow_date() {
        let tmr_date_with_time
        const tomorrow_date = moment().add(1, 'days');
        const tmr_date_format = tomorrow_date.format('YYYY-MM-DD')
        const nowTime = tomorrow_date.format('H:m')
        tmr_date_with_time = tmr_date_format + " " + nowTime;
        return tmr_date_with_time
    }


    enter_start_date_as_current_date() {
        let current_date_with_time = this.current_date()
        cy.xpath("//input[@class='form_datetime form-control' and @id='start_date']",{timeout:50000}).should("be.visible")
            .clear()
            .type(current_date_with_time)
    }

    enter_end_date_as_tomorrow_date() {
        let tomorrow_date_with_time = this.tomorrow_date()

        cy.xpath("//input[@class='form_datetime form-control' and @id='end_date']",{timeout:50000}).should("be.visible")
            .clear()
            .type(tomorrow_date_with_time)
    }

    click_select_user_button_on_module_invitation_page() {
        cy.xpath("//a[@id='select-employees-btn']",{timeout:50000})
            .should("be.visible").click()
    }

    select_activated_user_on_module_invitation_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//span[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[@class='cell btns']",{timeout:50000})
                .should("be.visible").click()
        })
    }

    click_validate_buttton_on_module_invitation_page() {
        cy.xpath("//a[@class='btn btn-primary save-btn']",{timeout:50000}).should("be.visible")
            .click()
    }

    click_invite_button_on_module_invitation_page() {
        cy.xpath("//button[@id='invite']",{timeout:50000}).should("be.visible").click();

    }
}

export default ModulesPage