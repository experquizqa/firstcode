import moment from "moment";

class TrainingsPage {

    //To click the trainings menu from side menu
    click_trainings_side_menu() {
        cy.xpath("//a[@href='/enterprise/trainings']//span[text()='Trainings']").click()
    }

    //To click the new trainings button at trainings page
    click_new_training_button() {
        cy.xpath("//a[@id='new-training-btn']", { timeout: 50000 })
            .should('be.visible')
            .click({ force: true })
    }

    //To enter training name at training creation page
    enter_training_name(trainingdata, randomNumber) {
        cy.xpath("//div[@class='bloc-body']//div//div//label[contains(text(),'Name')]//..//input[@id='training_name']", { timeout: 10000 })
            .should("be.visible").type(trainingdata["training_name"] + " " + randomNumber);
    }

    //To choose start date at training creation page
    enter_start_date_as_current_date_in_trainings() {
        let current_date_with_time = this.current_date()
        cy.xpath("//input[@id='start_date']",{ timeout: 10000 })            
            .should("be.visible").clear().type(current_date_with_time)
    }

    current_date() {
        const today_date = moment()
        const tdy_date_format = today_date.format('DD/MM/YYYY')
        return tdy_date_format
    }

    //To choose end date at training creation page
    enter_end_date_as_tomorrow_date_in_trainings() {
        let tomorrow = this.tomorrow_date()
        cy.xpath("//input[@id='end_date']",{ timeout: 10000 })            
            .should("be.visible").clear().type(tomorrow)
    }

   tomorrow_date() {
        const tomorrow_date = moment().add(1, 'days');
        const tmr_date_format = tomorrow_date.format('DD/MM/YYYY')
        return tmr_date_format
    }

    //To click trainer field at training creation page
    click_trainer_field() {
        cy.xpath("//input[@class='form-control' and @id='trainer_name']",{ timeout:10000 }).should("be.visible").click()
    }

    //To choose trainer at select user page of training creation page
    choose_trainer(admindatas) {
        let last_name = admindatas.last_name.toUpperCase();
        cy.xpath("//div[@class='user-item']//span[text()='" + last_name + " " + admindatas["first_name"][0].toUpperCase() + admindatas["first_name"].slice(1) + "']",{ timeout:20000 })
            .should("be.visible").click()
    }

    //To select cycles for participants at training creation page
    choose_cycles_for_participants(cycledata) {
        this.click_evaluation_cycle_for_participants_field();
        this.select_cycle_on_evaluation_cycle_for_participants_field(cycledata)
    }

    //To click the Evaluation cycle for participants filed
    click_evaluation_cycle_for_participants_field() {
        cy.xpath("//*[@id='cycle_select_participant-button']",{ timeout: 10000 }).should("be.visible").click()
    }

    //To select the none option at Evaluation cycle for participants filed
    select_none_option_at_evaluation_cycle_for_participants_field() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'None')]",{ timeout: 10000 }).first().should("be.visible").click()
    }

    //To select the cycle at the Evaluation cycle for participants filed
    select_cycle_on_evaluation_cycle_for_participants_field(cycledata) {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'" + cycledata["cycle_name"] + "')]",{ timeout: 10000 }).should("be.visible").click()
    }

    //To select cycles for trainer at training creation page
    choose_cycles_for_trainer(cycledata) {
        this.click_evaluation_cycle_for_trainer_field();
        this.select_cycle_on_evaluation_cycle_for_trainer_field(cycledata)
    }

    //To click the Evaluation cycle for trainer filed
    click_evaluation_cycle_for_trainer_field() {
        cy.xpath("//*[@id='cycle_select_trainer-button']",{ timeout: 10000 }).should("be.visible").click()
    }

    //To select the none option at Evaluation cycle for trainer filed
    select_none_option_at_evaluation_cycle_for_trainer_field() {
        cy.xpath("(//div[contains(@id,'ui-id')][contains(text(),'None')])[2]",{ timeout: 10000 }).should("be.visible").click()
    }

    //To select the cycle at the Evaluation cycle for trainer filed
    select_cycle_on_evaluation_cycle_for_trainer_field(cycledata) {
        cy.xpath("(//div[contains(@id,'ui-id')][contains(text(),'" + cycledata["cycle_name"] + "')])[2]",{ timeout: 10000 }).should("be.visible").click()
    }

    //To select cycles for managers at training creation page
    choose_cycles_for_managers(cycledata) {
        this.click_evaluation_cycle_for_managers_field();
        this.select_cycle_on_evaluation_cycle_for_managers_field(cycledata)
    }

    //To click the Evaluation cycle for managers filed
    click_evaluation_cycle_for_managers_field() {
        cy.xpath("//*[@id='cycle_select_manager-button']",{ timeout: 10000 }).should("be.visible").click()
    }

    //To select the none option at Evaluation cycle for managers filed
    select_none_option_at_evaluation_cycle_for_managers_field() {
        cy.xpath("(//div[contains(@id,'ui-id')][contains(text(),'None')])[3]",{ timeout: 10000 }).should("be.visible").click()
    }

    //To select the cycle at the Evaluation cycle for managers filed
    select_cycle_on_evaluation_cycle_for_managers_field(cycledata) {
        cy.xpath("(//div[contains(@id,'ui-id')][contains(text(),'" + cycledata["cycle_name"] + "')])[3]",{ timeout: 10000 }).should("be.visible").click()
    }


    //To click the save button at the training creation page
    click_save_button_at_training_creation_page() {
        cy.xpath("//button[@type='submit'][@form='training-edit-form']",{ timeout: 10000 }).should("be.visible").click()
    }

    //Verify the created trainings
    verify_created_training(training, randomNumber) {
        cy.xpath("//a[normalize-space()='" + training["training_name"] + " " + randomNumber + "']",{ timeout: 10000 })
            .should('be.visible')
    }

    //To open the created training
    open_created_training(basedatas, trainingdata,) {
        cy.xpath("//a[normalize-space()='" + trainingdata["training_name"] + "']",{ timeout: 10000 })
            .should("be.visible").click()
    }

    click_questionnaire_tab_on_opened_training() {
        cy.xpath("//a[text()='Questionnaires']",{ timeout: 10000 })
            .should("be.visible").click()
    }

    drag_questionnaire_to_practice_section_on_training(training_ques) {
        cy.wrap(training_ques).each(tq => {
            if (tq.usage === "free") {
                cy.xpath("//div[contains(text(),'" + tq["questionnaire_name"] + "')]",{ timeout: 10000 })
                    .trigger("mousedown", { which: 1 })
                cy.xpath("//*[@id='items-set-3']",{ timeout: 10000 })
                    .trigger("mousemove")
                    .trigger("mouseup", { force: true })
            }
        })
    }

    drag_questionnaire_to_used_section_on_training(training_ques) {
        cy.wrap(training_ques).each(tq => {
            if (tq.usage === "evaluation") {
                cy.xpath("//div[contains(text(),'" + tq["questionnaire_name"] + "')]",{ timeout: 10000 })
                    .trigger("mousedown", { which: 1 })
                cy.xpath("//*[@id='items-set-2']",{ timeout: 10000 })
                    .trigger("mousemove")
                    .trigger("mouseup", { force: true })
            }
        })
        cy.wait(5000)
    }

    drag_cycle_questionnaire_to_practice_section_on_training(cy_ques) {
        cy.wrap(cy_ques).each(cq => {
            if (cq.questionnaire_name === "second_questionnaires") {
                cy.xpath("//div[contains(text(),'" + cq["questionnaire_name"] + "')]",{ timeout: 10000 })
                    .trigger("mousedown", { which: 1 })
                cy.xpath("//*[@id='items-set-3']",{ timeout: 10000 })
                    .trigger("mousemove")
                    .trigger("mouseup", { force: true })
            }
        })
    }

    //To click the invitation tab
    click_invitation_tab() {
        cy.xpath("//a[text()='Invitations']", { timeout: 30000 }).should("be.visible").click()
    }

    click_participants_tab() {
        cy.xpath("//a[text()='Participants']", { timeout: 30000 }).should("be.visible").click()
    }

    drag_and_drop_user_on_present_section(user) {
        cy.wrap(user).each((data) => {
            cy.xpath("//div[contains(text(),'" + data["last_name"].toUpperCase() + " " + data["first_name"][0].toUpperCase() + data["first_name"].slice(1) + "')]")
                .trigger("mousedown", { which: 1 })
            cy.xpath("//*[@id='items-set-3']",{ timeout: 10000 })
                .trigger("mousemove")
                .trigger("mouseup", { force: true })
            cy.wait(5000)
        })
    }

    //To click the select participants button
    click_select_participants_button() {
        cy.xpath("//a[@id='select-employees-btn']",{ timeout: 10000 }).should("be.visible").click()
    }

    //Choose participants to training
    choose_participants(first_name, last_name) {
        cy.xpath("//span[@class='username'][contains(text(),'" + last_name.toUpperCase() + " " + first_name[0].toUpperCase() + first_name.slice(1) + "')]/../../div/a[@class='to-right-btn']",{ timeout: 10000 }).should("be.visible").click()
    }

    selected_participant_is_on_not_send_status(first_name, last_name) {
        cy.xpath("//div[@id='invitations-status-not-sent']/p[normalize-space(text())='" + last_name.toUpperCase() + " " + first_name[0].toUpperCase() + first_name.slice(1) + "']",{ timeout: 10000 }).should('be.exist')
    }

    //Click validate button
    click_validate_button() {
        cy.xpath("//a[@class='btn btn-primary save-btn']",{ timeout: 10000 }).should("be.visible").click()
    }

    verify_user_is_on_send_status_after_inviting_on_training(first_name, last_name) {
        cy.xpath("//div[@id='invitations-status-sent']/p[normalize-space(text())='" + last_name.toUpperCase() + " " + first_name[0].toUpperCase() + first_name.slice(1) + "']",{ timeout: 10000 }).should('be.exist')
    }

    click_send_invitation_button() {
        cy.xpath("//a[@id='invitations-send-btn']",{ timeout: 10000 })
            .should("be.visible").click()
    }

    click_result_tab_on_training() {
        cy.xpath("//a[text()='Results']",{ timeout: 10000 }).should("be.visible").click()
    }

    click_evaluation_tab_on_training_result_page() {
        cy.xpath("//a[text()='Evaluations']",{ timeout: 10000 }).should("be.visible").click()
    }

    click_report_tab_on_training_result_page() {
        cy.xpath("//a[text()='Report']",{ timeout: 10000 }).should("be.visible").click()
    }

    click_cycle_tab_on_training_result_page() {
        cy.xpath("//a[text()='Cycles']",{ timeout: 10000 }).should("be.visible").click()
    }

    click_survey_tab_on_training() {
        cy.xpath("//a[text()='Surveys']",{ timeout: 10000 }).should("be.visible").click()
    }

    verify_practice_training_result(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[text()='" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "']/..//strong[text()='1']/..//span[text()='Practice']",{ timeout: 10000 })
                .should('be.exist')
        })
    }

    verify_evaluation_training_result_on_report_tab(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[text()='" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "']/..//strong[text()='1']/..//span[normalize-space(text())='Evaluation']",{ timeout: 10000 })
                .should('be.exist')
        })

    }

    verify_cycle_result_on_training_result_page(userdata, admin) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[text()='" + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + " " + user["last_name"][0].toUpperCase() + user["last_name"].slice(1) + "']",{ timeout: 10000 })
                .should('be.exist')
        })
        cy.xpath("//div[text()='" + admin["first_name"][0].toUpperCase() + admin["first_name"].slice(1) + " " + admin["last_name"][0].toUpperCase() + admin["last_name"].slice(1) + "']",{ timeout: 10000 })
            .should('be.exist')

    }

    verify_survey_result_on_training_page(form) {
        cy.xpath("(//div[contains(normalize-space(text()),'" + form["form_name"] + "')]/..//strong[text()='1/1'])[1]",{ timeout: 10000 })
            .should('be.exist')
        cy.xpath("(//div[contains(normalize-space(text()),'" + form["form_name"] + "')]/..//strong[text()='1/1'])[2]",{ timeout: 10000 })
            .should('be.exist')
        cy.xpath("(//div[contains(normalize-space(text()),'" + form["form_name"] + "')]/..//strong[text()='1/1'])[3]",{ timeout: 10000 })
            .should('be.exist')
    }


    verify_evaluation_training_result_on_evaluation_tab(training_ques) {
        cy.wrap(training_ques).each(training => {
            if (training.usage === "evaluation")
                cy.xpath("//a[normalize-space(text())='" + training["questionnaire_name"] + "']/../..//strong[text()='1/1']",{ timeout: 10000 })
                    .should('be.exist')
        })

    }

    click_see_as_trainer_button() {
        cy.xpath("//div[@class='top-right']//a[@class='btn btn-default'][text()='See as trainer']",{ timeout: 10000 }).should("be.visible").click()
    }

    //Click see as admin button at the training page
    click_see_as_admin_button() {
        cy.xpath("//div[@class='top-right']//a[@class='btn btn-default'][text()='See as admin']",{ timeout: 10000 }).should("be.visible").click()
    }

    invite_questionnaire_as_evaluation_on_training() {
        cy.xpath("//span[normalize-space(text())='Used']/../../..//a[@title='Submit evaluation to participants']",{ timeout: 10000 })
            .should("be.visible").click()
        cy.xpath("//button[@form='questionnaire_submit_form']",{ timeout: 10000 })
            .should("be.visible").click()
    }

    click_start_of_cycle_mode_on_training_create_page() {
        cy.xpath("//span[@id='cycle_t0_select-button']",{ timeout: 10000 }).should("be.visible").click()
    }

    select_when_inviting_option_on_start_of_cycle() {
        cy.xpath("//div[text()='When inviting']",{ timeout: 10000 }).should("be.visible").click()
    }

    click_cycle_invitation_button_on_training() {
        cy.xpath("//a[@id='cycle-invite-btn']",{ timeout: 10000 }).should("be.visible").click()
    }

    click_user_view_menu_on_admin() {
        cy.xpath("//span[text()='User view']",{ timeout: 10000 }).should("be.visible").click()
    }

}

export default TrainingsPage