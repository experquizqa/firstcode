class SurveysPage {
//To click the surveys menu from side menu
    click_surveys_side_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Surveys")
            .click({force:true})
    }

    //To click the new form button at survey page
    click_new_form_button() {
        cy.xpath("//a[@id='new-survey-btn'][normalize-space(text()='New Form')]",{timeout:50000})
            .should("be.visible").click()
    }

    //To enter survey name at survey creation page
    enter_survey_name(surveydata) {
        let form_name = surveydata.form_name;
        cy.xpath("//div[@class='bloc-body']//div//div//label[normalize-space(text()='Name of the form')]//..//input[@id='form_name']",{timeout:50000})
            .should("be.visible").type(form_name)
    }

    //To click the save button at the survey creation page
    click_save_button_at_survey_creation_page() {
        cy.xpath("//button[@type='submit'][@name='submit']",{timeout:50000}).should("be.visible").click()
        cy.reload()
    }

    //To open the created survey
    open_created_survey(surveydata) {
        cy.xpath("//span[text()='" + surveydata["form_name"] + "']",{timeout:50000})
            .should("be.visible").click()
    }

    //To click the edit button at the surveys overview page
    click_edit_button() {
        cy.xpath("//div[@id='std-toolbar']//a[@title='Edit survey form']",{timeout:50000})
            .should("be.visible").click()
    }

    create_all_fields_on_created_surveys(surveydata) {
        cy.wrap(surveydata["fields"]).each(field => {
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down" || field["field_type"] === "label" || field["field_type"] === "section_separator") {
                this.move_field(field["field_type"])
                this.edit_field(field["field_type"])
            }
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.common_fields(field)
            }
            if (field["field_type"] === "label") {
                this.label_field(field)
            }
            if (field["field_type"] === "section_separator") {
                this.section_separator_fields(field)
            }
            if (field["field_type"] === "media") {
                this.move_media_fields(field["field_type"])
                this.upload_media(field)
            }
            if (field["field_type"] === "text" || field["field_type"] === "para") {
                this.enter_minimum_length(field)
                this.enter_maximum_length(field)
            }
            if (field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.enter_choices(field)
            }
            if (field["field_type"] !== "media") {
                this.click_save_field()
            }
        })

    }

    move_field(field) {
        if (field === 'text') {
            this.move_text_field()
        }
        if (field === 'para') {
            this.move_text_area()
        }
        if (field === 'number') {
            this.move_number_field()
        }
        if (field === 'date') {
            this.move_date_field()
        }
        if (field === 'selector') {
            this.move_selector_field()
        }
        if (field === 'bool') {
            this.move_bool_field()
        }
        if (field === 'drop_down') {
            this.move_drop_down_field()
        }
        if (field === 'multiple_select') {
            this.move_multiple_select_field()
        }
        if (field === 'label') {
            this.move_label_field()
        }
        if (field === 'section_separator') {
            this.move_section_separator_field()
        }
    }

    edit_field(field) {
        if (field === 'text') {
            this.edit_text_field()
        }
        if (field === 'para') {
            this.edit_text_area()
        }
        if (field === 'number') {
            this.edit_number_field()
        }
        if (field === 'date') {
            this.edit_date_field()
        }
        if (field === 'selector') {
            this.edit_selector_field()
        }
        if (field === 'bool') {
            this.edit_bool_field()
        }
        if (field === 'drop_down') {
            this.edit_drop_down_field()
        }
        if (field === 'multiple_select') {
            this.edit_multiple_select_field()
        }
        if (field === 'label') {
            this.edit_label_field()
        }
        if (field === 'section_separator') {
            this.edit_section_separator_field()
        }
    }

    move_text_field() {
        cy.xpath("(//div[contains(@class,'field-type-text')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_text_area() {
        cy.xpath("(//div[contains(@class,'field-type-textarea')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_date_field() {
        cy.xpath("(//div[contains(@class,'field-type-date')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_number_field() {
        cy.xpath("(//div[contains(@class,'field-type-number')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_selector_field() {
        cy.xpath("(//div[contains(@class,'field-type-select')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_bool_field() {
        cy.xpath("(//div[contains(@class,'field-type-bool')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_drop_down_field() {
        cy.xpath("(//div[contains(@class,'field-type-dropdown')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_multiple_select_field() {
        cy.xpath("(//div[contains(@class,'field-type-multiselect')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_section_separator_field() {
        cy.xpath("(//div[contains(@class,'field-type-section')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    move_label_field() {
        cy.xpath("(//div[contains(@class,'field-type-label')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    edit_text_field() {
        let element = "(//div[contains(@class,'field-type-text')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-text')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })


    }

    edit_text_area() {
        let element = "(//div[contains(@class,'field-type-textarea')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-textarea')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })

    }

    edit_date_field() {
        let element = "(//div[contains(@class,'field-type-date')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-date')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })

    }

    edit_number_field() {
        let element = "(//div[contains(@class,'field-type-number')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-number')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')

            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })

    }

    edit_selector_field() {
        let element = "(//div[contains(@class,'field-type-select')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-select')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })
    }

    edit_bool_field() {
        let element = "(//div[contains(@class,'field-type-bool')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-bool')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })
    }

    edit_drop_down_field() {
        let element = "(//div[contains(@class,'field-type-dropdown')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-dropdown')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })
    }

    edit_multiple_select_field() {
        let element = "(//div[contains(@class,'field-type-multiselect')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-multiselect')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })
    }

    edit_section_separator_field() {
        let element = "(//div[contains(@class,'field-type-section')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-section')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })
    }

    edit_label_field() {
        let element = "(//div[contains(@class,'field-type-label')]//a[contains(@title,'Edit settings')])"
        cy.xpath("(//div[contains(@class,'field-type-label')]//a[contains(@title,'Edit settings')])",{timeout:50000})
            .each((el, index, array) => {
                let xpath_index_count = array.length
                cy.wrap(xpath_index_count).as('MyIndexCount')
            })
        cy.get('@MyIndexCount',{timeout:50000}).then(count => {
            cy.xpath(element + "[" + count + "]")
                .should("be.visible").click()
        })

    }

    common_fields(field) {
        this.enter_field_names(field)
        this.enter_label_name(field)
        this.select_input_required(field)
        this.select_user_data(field)
    }

    enter_field_names(field) {
        cy.on('window:confirm', () => true);
        cy.xpath("//input[@id='field_name']", {timeout: 500000})
            .should('be.visible')
            .clear()
            .type(field["field_name"])
    }

    enter_label_name(field) {
        cy.xpath("//input[@id='label']",{timeout:50000})
            .should("be.visible").type(field["label_name"])
    }

    select_input_required(field) {
        cy.xpath("//input[@id='required']/../label/span[@class='checked']",{timeout:50000})
            .should("be.visible").click()
    }

    select_user_data(field) {
        cy.xpath("//input[@id='user_data']/../label/span[@class='checked']",{timeout:50000})
            .should("be.visible").click()
    }

    label_field(field) {
        this.enter_label_name(field)
    }

    section_separator_fields(field) {
        this.enter_field_names(field)
        this.enter_label_name(field)
    }

    move_media_fields(field) {
        if (field === 'media') {
            this.move_media_field()
        }
    }

    move_media_field() {
        cy.xpath("(//div[contains(@class,'field-type-media')]//a[contains(@title,'form')])[1]",{timeout:50000})
            .should("be.visible").click()
    }

    upload_media(field) {
        this.upload_media_field(field)
    }

    upload_media_field(file_name) {
        cy.xpath("(//div[contains(@class,'field-type-media')]//div[@class='picto-wrapper'])[2]",{timeout:50000})
            .should("be.visible").click()
        cy.xpath("//a[text()='Enterprise']", {timeout: 50000})
            .should("be.visible").click()
        cy.xpath("//div[@class='filename']//span[text()='" + file_name["name"] + "']", {timeout: 50000})
            .should("be.visible").click()
    }

    enter_minimum_length(length) {
        cy.xpath("//*[@id='min_length']",{timeout:50000})
            .should("be.visible").type(length["minimum_length"])
    }

    enter_maximum_length(length) {
        cy.xpath("//*[@id='max_length']",{timeout:50000})
            .should("be.visible").type(length["maximum_length"])
    }

    click_save_field() {
        cy.xpath("//*[@id='field-settings-done-btn']",{timeout:50000})
            .should("be.visible").click()
    }

    enter_choices(field) {
        cy.wrap(field["choices"]).then(choices => {
            cy.xpath("//*[@id='choices']",{timeout:50000})
                .clear()
            cy.xpath("//*[@id='choices']",{timeout:50000})
                .should("be.visible").type(choices.toString())
        })
    }

    //To verify the success message after saved surveys
    verify_success_message_after_survey_saved(surveydata) {
        let form_name = surveydata.form_name;
        cy.xpath("//div[@class='xq-flashes xq-m collapse']//ul//li[normalize-space(text()='  Survey form " + form_name + " was successfully created. You may now define the fields.')]",{timeout:50000})
            .should('be.visible');
    }

    click_trial_play_on_survey_list_page(surveydata) {
        cy.xpath("//*[contains(text(),'" + surveydata["form_name"] + "')]/../..//a[@title='Try survey form']",{timeout:50000})
            .should("be.visible").click()
    }

    fill_form(surveydata) {
        cy.wrap(surveydata["fields"]).each(field => {
            if (field["field_type"] === "selector" || field["field_type"] === "bool") {
                this.select_choice(field)
            }
            if (field["field_type"] === "drop_down") {
                this.select_option_from_drop_down_field(field)
            }
            if (field["field_type"] === "multiple_select") {
                this.select_multiple_1(field)
                this.select_multiple_2(field)
                this.select_multiple_3(field)
            } else {
                this.enter_text_field(field)
            }
        })
        this.submit_form()
    }

    fill_form_with_condition(conditiondata) {
        cy.wrap(conditiondata["fields"]).each(field => {
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number") {
                this.enter_text_field(field)
            }
            if (field["field_type"] === "selector" || field["field_type"] === "bool") {
                this.select_choice(field)
            }
            if (field["field_type"] === "drop_down") {
                this.select_option_from_drop_down_field(field)
            }
            if (field["field_type"] === "multiple_select") {
                this.select_multiple_1(field)
                this.select_multiple_2(field)
                this.select_multiple_3(field)
            }
        })
        this.submit_form()
    }

    select_choice(field) {
        if (field["answer_data"] !== "") {
            cy.xpath("//input[@name='" + field["field_name"] + "']/..//label[text()='" + field["answer_data"] + "']", {timeout: 5000})
                .should("be.visible").click()
        }
    }

    select_option_from_drop_down_field(field) {
        cy.xpath("//span[@id='drop_down_1-button']",{timeout:50000})
            .should("be.visible").click()
        cy.xpath("//div[text()='" + field["answer_data"] + "']",{timeout:50000})
            .should("be.visible").click()
    }

    select_multiple_1(field) {
        cy.xpath("//label[contains(text(),'" + field["answer_data_1"] + "')]",{timeout:50000})
            .should("be.visible").click()
    }

    select_multiple_2(field) {
        cy.xpath("//label[contains(text(),'" + field["answer_data_2"] + "')]",{timeout:50000})
            .should("be.visible").click()
    }

    select_multiple_3(field) {
        cy.xpath("//label[contains(text(),'" + field["answer_data_3"] + "')]",{timeout:50000})
            .should("be.visible").click()
    }

    enter_text_field(field) {
        if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number") {
            cy.xpath("//*[@name='" + field["field_name"] + "']",{timeout:50000})
                .should("be.visible").type(field["answer_data"])
            // cy.xpath("//body")
            cy.xpath("//div[@class='left-column']",{timeout:50000})
                .should("be.visible").click()
        }
    }

    submit_form() {
        cy.xpath("//*[@name='submit']",{timeout:50000})
            .should("be.visible").click()
    }

    create_fields_with_condition(conditiondata) {
        cy.wrap(conditiondata["fields"]).each(field => {
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down" || field["field_type"] === "label" || field["field_type"] === "section_separator") {
                this.move_field(field["field_type"])
                this.edit_field(field["field_type"])
            }
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.common_fields(field)
            }
            if (field["field_type"] === "label") {
                this.label_field(field)
            }
            if (field["field_type"] === "section_separator") {
                this.section_separator_fields(field)
            }
            if (field["field_type"] === "media") {
                this.move_media_fields(field["field_type"])
                this.upload_media(field)
            }
            if (field["field_type"] === "text" || field["field_type"] === "para") {
                this.enter_minimum_length(field)
                this.enter_maximum_length(field)
            }
            if (field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "multiple_select" || field["field_type"] === "drop_down") {
                this.enter_choices(field)
            }
            if (field["field_type"] !== "media") {
                this.click_save_field()
            }
            this.enter_condition_for_each_field(field)
        })

    }

    enter_condition_for_each_field(field) {
        if (field["field_type"] === "para") {
            this.edit_text_area()
            this.enter_condition_for_para(field)
        }
        if (field["field_type"] === "date") {
            this.edit_date_field()
            this.enter_condition_for_date(field)
        }
        if (field["field_type"] === "number") {
            this.edit_number_field()
            this.enter_condition_for_number(field)
        }
        if (field["field_type"] === "selector") {
            this.edit_selector_field()
            this.enter_condition_for_selector(field)
        }
        if (field["field_type"] === "bool") {
            this.edit_bool_field()
            this.enter_condition_for_bool(field)
        }
        if (field["field_type"] === "drop_down") {
            this.edit_drop_down_field()
            this.enter_condition_for_drop_down(field)
        }
        if (field["field_type"] === "multiple_select") {
            this.edit_multiple_select_field()
            this.enter_condition_for_multiple(field)
        }
        if (field["field_type"] === "multiple_select" || field["field_type"] === "drop_down" || field["field_type"] === "bool" || field["field_type"] === "selector" || field["field_type"] === "number" || field["field_type"] === "date" || field["field_type"] === "para")
            this.click_save_field()
    }

    enter_condition_for_para(field) {
        cy.xpath("//input[@id='condition']",{timeout:50000})
            .should("be.visible").type(field["condition_para"])
    }

    enter_condition_for_date(field) {
        cy.xpath("//input[@id='condition']",{timeout:50000})
            .should("be.visible").type(field["condition_date"])
    }

    enter_condition_for_number(field) {
        cy.xpath("//input[@id='condition']",{timeout:50000})
            .should("be.visible").type(field["condition_number"])
    }

    enter_condition_for_selector(field) {
        cy.xpath("//input[@id='condition']",{timeout:50000})
            .should("be.visible").type(field["condition_selector"])
    }

    enter_condition_for_bool(field) {
        cy.xpath("//input[@id='condition']",{timeout:50000})
            .should("be.visible").type(field["condition_bool"])
    }

    enter_condition_for_drop_down(field) {
        cy.xpath("//input[@id='condition']",{timeout:50000})
            .should("be.visible").type(field["condition_drop_down"])
    }

    enter_condition_for_multiple(field) {
        cy.xpath("//input[@id='condition']",{timeout:50000})
            .should("be.visible").type(field["condition_multiple"])
    }

    click_invitation_tab_on_survey() {
        cy.xpath("//a[text()='Invitations']",{timeout:50000})
            .should("be.visible").click()
    }

    click_select_user_button_on_survey_invitation_page() {
        cy.xpath("//a[@id='select-employees-btn']",{timeout:50000})
            .should("be.visible").click()
    }

    click_email_and_mobile_button_on_survey_invitation_page() {
        cy.xpath("//a[@id='select-contacts-btn']",{timeout:50000})
            .should("be.visible").click()
    }

    select_activated_user_on_survey_invitation_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//span[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[@class='cell btns']",{timeout:50000})
                .should("be.visible").click()
        })
    }

    enter_user_mailid_on_email_and_mobile_popup(contactuser) {
        cy.xpath("//div[@id='select-contacts']//textarea[@id='contacts-participants']",{timeout:50000})
            .should("be.visible").type(contactuser["contact_user_id"])
    }

    click_validate_buttton_on_survey_invitation_page() {
        cy.xpath("//a[@class='btn btn-primary save-btn']",{timeout:50000})
            .should("be.visible").click()
    }

    click_save_button_on_email_and_mobile_popup() {
        cy.xpath("//a[@id='save-contacts']",{timeout:50000})
            .should("be.visible").click()
    }

    click_invite_buttton_on_survey_invitation_page() {
        cy.xpath("//button[@name='invite-btn']",{timeout:50000})
            .should("be.visible").click()
    }

    click_result_tab_on_survey() {
        cy.xpath("//a[text()='Results']",{timeout:50000})
            .should("be.visible").click()
    }

    verify_status_on_played_survey_on_survey_result_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/..//div[contains(text(),'completed')]",{timeout:50000})
                .should('be.exist')
        })
    }

    verify_status_on_played_contact_user_on_survey_result_page(contactuser) {
        cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/..//div[contains(text(),'completed')]",{timeout:50000})
            .should('be.exist')
    }

    click_detail_button_on_survey_result_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/..//a[@title='Detail']",{timeout:50000})
                .should("be.visible").click()
        })
    }

    click_detail_button_on_contact_user_of_survey_result_page(contactuser) {
        cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/..//a[@title='Detail']",{timeout:50000})
            .should("be.visible").click()
    }

    verify_result_data_on_survey(surveydata, userdata) {
        cy.wrap(userdata).each(user => {
            cy.wrap(surveydata["fields"]).each(field => {
                if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "drop_down") {
                    cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data"] + "']",{timeout:50000})
                        .should('be.exist')
                }
                if (field["field_type"] === "multiple_select") {
                    cy.xpath("//div[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"] + "')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data_2"] + ", " + field["answer_data_1"] + ", " + field["answer_data_3"] + "']", {timeout: 50000})
                        .should('be.exist')
                }
            })
        })
    }


    verify_result_data_for_contactuser_on_survey(surveydata, contactuser) {
        cy.wrap(surveydata["fields"]).each(field => {
            if (field["field_type"] === "text" || field["field_type"] === "para" || field["field_type"] === "date" || field["field_type"] === "number" || field["field_type"] === "selector" || field["field_type"] === "bool" || field["field_type"] === "drop_down") {
                cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data"] + "']")
                    .should('be.exist')
            }
            if (field["field_type"] === "multiple_select") {
                cy.xpath("//div[contains(text(),'[" + contactuser["contact_user_id"] + "]')]/../..//div[text()='" + field["field_name"] + "']/..//div[text()='" + field["answer_data_2"] + ", " + field["answer_data_1"] + ", " + field["answer_data_3"] + "']", {timeout: 50000})
                    .should('be.exist')
            }
        })

    }

    verify_contact_user_is_on_the_return_link_page_after_completed_form(link) {
        cy.wait(50000)
        cy.url().should('eq', link["redirect_url"])
        cy.title().should('eq', 'Google')
        cy.wait(5000)
        cy.go('back')
    }

}

export default SurveysPage