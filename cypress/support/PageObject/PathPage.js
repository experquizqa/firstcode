class PathPage {

    //To click the path menu from side menu
    click_path_side_menu() {
        cy.xpath("//div[@class='nav-wrapper']//a[@href='/enterprise/paths/list']//span[text()='Paths']", {timeout: 30000}).click()            
    }

    //To click the new path button at path page
    click_new_path_button() {
        cy.xpath("//a[@id='new-path-btn'][normalize-space(text()='New Path')]",{timeout: 50000}).should("be.visible")
            .click()
    }

    //To enter path name at path creation page
    enter_path_name(pathdata) {
        let path_name = pathdata.path_name;
        cy.xpath("//div[@class='bloc-body']//div//div//label[contains(text(),'Name')]//..//input[@id='path_name']",{timeout: 50000}).should("be.visible")
            .type(path_name)
    }

    //To click the save button at the path creation page
    click_save_button_at_path_creation_page() {
        cy.xpath("//button[@type='submit'][@form='path-form']",{timeout: 50000}).should("be.visible").click()
    }

    //To open the created path
    open_created_path(pathdata) {
        let path_name = pathdata.path_name;
        cy.xpath("//a[contains(@class,'cell path-name')][contains(normalize-space(.),'" + path_name + "')]", {timeout: 30000})
            .click()
    }

    //To click the modules tab at path overview page
    click_modules_tab() {
        cy.xpath("//div[@class='main-content path-view-page']//div//ul//li//a[text()='Modules']",{timeout: 50000}).should("be.visible")
            .click()
    }

    //To filter created base on modules page
    filter_created_base_on_element_page(basedatas) {
        this.click_topic_filter_field()
        this.select_all_topic_option()
        this.click_topic_filter_field()
        this.select_base_on_topic_filter_field(basedatas)
    }

    //To click the topic filter
    click_topic_filter_field() {
        cy.xpath("//*[@id='topic-filter-button']",{timeout: 50000}).should("be.visible").click();
    }

    //To select all options
    select_all_topic_option() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All topics')]",{timeout: 50000}).should("be.visible").click();
    }

    //To select the base on the topic filter
    select_base_on_topic_filter_field(basedatas) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + basedatas["base_name"] + "']",{timeout: 50000}).should("be.visible").click();
    }

    //To add the module to the path
    select_module_to_path(module_name) {
        cy.xpath("//div[contains(@class,'module-line')]//div[contains(text(),'" + module_name + "')]")
            .trigger("mousedown", {which: 1})
        cy.get('#selected-modules')
            .trigger("mousemove")
            .trigger("mouseup", )
    }

    //To click save button at add module page of path
    click_save_button_at_add_module_page() {
        cy.xpath("//a[@id='save-and-quit'][text()='Save']",{timeout: 50000}).should("be.visible").click()
    }

    //To verify success message after added module
    verify_success_message_after_added_module_into_path(module_name) {
        cy.xpath("//div[@class='xq-flashes xq-m collapse']//ul//li[normalize-space(text()='Your path " + module_name + " has been successfully saved')]",{timeout: 50000}).should("be.visible")
    }
}

export default PathPage