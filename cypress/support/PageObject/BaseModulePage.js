import * as path from "path";
import MediaPage from "./MediaPage";

const mediapage = new MediaPage()

class BaseModulePage {
    enterQuestinnaireName(questionnaire_name) {
        cy.get('#questionnaire_name',{ timeout: 10000 }).should("be.visible").type(questionnaire_name)
    }

    enterQuestionCount(count) {
        cy.get("input[id='nb_questions'][name='nb_questions']",{ timeout: 10000 }).should("be.visible").type(count)
    }

    saveButton() {
        cy.get("button[name='submit'][type='submit']",{ timeout: 10000 }).should("be.visible").click()
    }

    clickVisibility(questionnaireName) {
        cy.xpath("//a[contains(text(),'" + questionnaireName + "')]/..//a[@title='Define visibility']",{ timeout: 10000 })
        .should("be.visible").click()
    }

    MakeItPublic(questionnaireName) {
        cy.xpath("//div[div/span[contains(text(),'Public access')]]/../div/h2/a[contains(text(),'Make it public')]",{ timeout: 10000 }).should("be.visible").click()
        cy.xpath("//input[@name='public_link']",{ timeout: 10000 }).should("be.visible").type(questionnaireName)
        cy.xpath("//button[@id='remove-public']/../button[contains(text(),'Make Public')]",{ timeout: 10000 }).should("be.visible").click()
    }

    MakeItVisible() {
        cy.xpath("//label[@for='can_view_all']/span[contains(@class,'fa fa-square-o')]",{ timeout: 10000 }).should("be.visible").click()
        this.saveButton()
    }

    clickTestSettings(questionnaireName) {
        cy.xpath("//a[contains(text(),'" + questionnaireName + "')]/..//a[@title='Define the test settings']",{ timeout: 10000 }).should("be.visible").click()
    }

    MakeRenewal() {
        cy.get("#renewal_period",{ timeout: 10000 }).should("be.visible").type("10")
        cy.xpath("(//button[@type='submit'])[1]",{ timeout: 10000 }).should("be.visible").click()
    }

    MakeForm(name) {
        cy.get("#survey_form_select-button",{ timeout: 10000 }).should("be.visible").click()
        cy.xpath("//ul[@aria-hidden='false']/li[@class='ui-menu-item']/div[text()='" + name + "']",{ timeout: 10000 }).click({force:true})
        cy.xpath("(//button[@type='submit'])[1]",{ timeout: 10000 }).should("be.visible").click()
    }

    MakeConsequence() {
        cy.wait(2000)
        cy.get("#messages_consequence_translated",{ timeout: 10000 }).scrollIntoView().type('if global_score >= 60 then message("Congratulations [first_name] [last_name]")')
        cy.xpath("(//button[@type='submit'])[1]",{ timeout: 10000 }).should("be.visible").click()
    }

    enter_survey_name(surveyName) {
        cy.xpath("//div[@class='bloc-body']//div//div//label[normalize-space(text()='Name of the form')]//..//input[@id='form_name']",{ timeout: 10000 }).should("be.visible")
            .type(surveyName)
    }

    clickDuplicate(baseName) {
        cy.xpath("//a[contains(text(),'" + baseName + "')]/..//li[contains(@class,'duplicate')]/a",{ timeout: 10000 }).should("be.visible").click()
        cy.xpath("//span[text()='Yes']",{ timeout: 10000 }).should("be.visible").click()
        cy.get("#clone-validate-btn",{ timeout: 10000 }).should("be.visible").click()
    }

    clickCreatedDuplicate(baseName) {
        const d = new Date().toISOString().split('T')[0]
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + baseName + " (copy) " + d + "')]",{ timeout: 10000 }).should("be.visible").click()

    }

    clickCreatedTransferBase(baseName) {
        const d = new Date().toISOString().split('T')[0]
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + baseName + " (transfer) " + d + "')]",{ timeout: 10000 }).should("be.visible").click()
    }

    verifyDuplicatedQuestionnaireModes() {
        cy.wait(10000)
        cy.reload()

        if (cy.xpath("//a[contains(text(),'Questionnaire public')]/..//span[@title='Public']").should("not.exist")) {
            cy.log("Public mode is removed from questionnaire in duplicated Base")
        } else {
            cy.log("Public mode is not removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire visibility')]/..//span[@title='Visible']").should("not.exist")) {
            cy.log("Visible mode is removed from questionnaire in duplicated Base")
        } else {
            cy.log("Visible mode is not removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire form')]/..//span[@title='Form/Survey']", {timeout: 50000}).should("be.visible")) {
            cy.log("Form mode is not removed from questionnaire in duplicated Base")
        } else {
            cy.log("Form mode is removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire consequence')]/..//span[@title='consequences']", {timeout: 50000}).should("be.visible")) {
            cy.log("consequences mode is not removed from questionnaire in duplicated Base")
        } else {
            cy.log("consequences mode is removed from questionnaire in duplicated Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire renewal')]/..//span[contains(@title,'Renewal period')]", {timeout: 50000}).should("be.visible")) {
            cy.log("Renewal mode is not removed from questionnaire in duplicated Base")
        } else {
            cy.log("Renewal mode is removed from questionnaire in duplicated Base")
        }

    }

    openCreatedSurvey(surveydata) {
        cy.xpath("//a[@class='cell form-name']/span[text()='" + surveydata + "']",{ timeout: 10000 }).should("be.visible").click()
    }

    clickOverviewTab() {
        cy.xpath("//ul[contains(@class,'compact')]/li/a[text()='Overview']",{ timeout: 10000 }).should("be.visible").click()
    }

    getQuestionCount() {
        let txt
        cy.wait(3000)
        cy.xpath("//label[text()='Questions']/../div", {timeout: 50000}).then($el => {
            txt = $el[0].innerText
            cy.writeFile('cypress/fixtures/questionData.json', '{"questionCount":' + txt + '}')
        })

    }

    getQuestionnaireCount() {
        let txt
        cy.wait(3000)
        cy.xpath("//label[text()='Questionnaires']/../div", {timeout: 50000}).then($el => {
            // cy.log($el[0].innerText)
            txt = $el[0].innerText
            cy.writeFile('cypress/fixtures/questionnaireData.json', '{"questionnaireCount":' + txt + '}')
        })
    }

    clickEditIconOfQuestion(count) {
        cy.xpath("(//a[@class='fa fa-pencil'])[" + count + "]",{ timeout: 10000 }).should("be.visible").click()
    }

    //choose a status in Edit question page
    chooseStatus(status) {
        cy.get("#workflow_status-button",{ timeout: 10000 }).should("be.visible").click()
        cy.xpath("//div[text()='" + status + "']",{ timeout: 10000 }).should("be.visible").click()
    }

    //choose a status in Base - question tab
    changeStatusAndVerifyCopied(status) {
        cy.get("#status-filter-button",{ timeout: 10000 }).should("be.visible").click()
        cy.xpath("//div[text()='" + status + "']",{ timeout: 10000 }).should("be.visible").click()

        if (status === "Draft" || status === "Proposed" || status === "Reviewed" || status === "Inactive") {
            cy.get("h2").contains("There are no questions matching your filter settings.").then((ele) => {
                if (cy.get("h2").contains("There are no questions matching your filter settings.").should("be.visible")) {
                    cy.log("PASS:" + status + " type questions are not copied")
                } else {
                    cy.log("FAIL:" + status + " type questions are copied")
                }
            })
        }
        if (status === "Active") {
            let countOfElements = 0;
            cy.get(".flex-row.body", {timeout: 50000}).then($elements => {
                let countOfElements = $elements.length;
                if (countOfElements === 7) {
                    cy.log("PASS:" + status + " type questions are copied")
                } else {
                    cy.log("Fail:" + status + " type questions are not copied")
                }
            });
        }
    }

    changeStatusAndVerifyCopiedInTransfer(status) {
        cy.get("#status-filter-button",{ timeout: 10000 }).should("be.visible").click()
        cy.xpath("//div[text()='" + status + "']",{ timeout: 10000 }).should("be.visible").click()

        if (status === "Draft" || status === "Proposed" || status === "Reviewed" || status === "Inactive") {
            cy.get('.question-div > .flex-row > .status').should("contain", status.toLowerCase())
        }
        if (status === "Active") {
            let countOfElements = 0;
            cy.get(".flex-row.body",{ timeout: 10000 }).should("be.visible").then($elements => {
                let countOfElements = $elements.length;
                if (countOfElements === 7) {
                    cy.log("PASS:" + status + " type questions are copied")
                } else {
                    cy.log("Fail:" + status + " type questions are not copied")
                }
            });
        }
    }

    clickSaveAndQuit() {
        cy.xpath("//a[@name='save']",{ timeout: 10000 }).should("be.visible").click()
    }

    ClickTransferButton() {
        cy.get("#topic-gcs-copy-btn",{ timeout: 10000 }).should("be.visible").click()
    }

    EnterExportFilName(randomNumber) {
        cy.get("#file_name",{ timeout: 10000 }).should("be.visible").type("Transfer" + randomNumber)
    }

    ClickBuildTransferButton() {
        cy.get("#topic-gcs-copy-modal .ajax-form>div.btn-toolbar>button.btn.btn-primary",{ timeout: 10000 }).scrollIntoView().should("be.visible").click()
    }

    getTransferLink() {
        cy.get('.copy-url').then(($url) => {
            const link = $url.attr('data-url')
            cy.task('setCopiedLink', link)
        });
    }

    visitLinkToTransfer() {
        cy.task('getCopiedLink').then(($url) => {
            cy.visit($url)
        })
    }

    verifyBaseTransfered(baseName) {
        const d = new Date().toISOString().split('T')[0]
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + baseName + " (transfer) " + d + "')]",{ timeout: 10000 }).should("be.visible").click()
        }

    verifyTransferedQuestionnaireModes() {
        cy.wait(10000)
        cy.reload()

        if (cy.xpath("//a[contains(text(),'Questionnaire public')]/..//span[@title='Public']").should("not.exist")) {
            cy.log("Public mode is removed from questionnaire in Transfered Base")
        } else {
            cy.log("Public mode is not removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire visibility')]/..//span[@title='Visible']").should("not.exist")) {
            cy.log("Visible mode is removed from questionnaire in Transfered Base")
        } else {
            cy.log("Visible mode is not removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire form')]/..//span[@title='Form/Survey']").should("not.exist")) {
            cy.log("Form mode is  removed from questionnaire in Transfered Base")
        } else {
            cy.log("Form mode is not removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire consequence')]/..//span[@title='consequences']",{ timeout: 10000 }).should("be.visible")) {
            cy.log("consequences mode is not removed from questionnaire in Transfered Base")
        } else {
            cy.log("consequences mode is removed from questionnaire in Transfered Base")
        }

        if (cy.xpath("//a[contains(text(),'Questionnaire renewal')]/..//span[contains(@title,'Renewal period')]", {timeout: 50000}).should("be.visible")) {
            cy.log("Renewal mode is not removed from questionnaire in Transfered Base")
        } else {
            cy.log("Renewal mode is removed from questionnaire in Transfered Base")
        }
    }

    verifyMediaExist(mediadata) {
        cy.wrap(mediadata).each((datas) => {
            cy.wrap(datas).then((ele) => {
                let media_name = ele.name;
                mediapage.verify_uploaded_media_is_exist(media_name)
            })
        })
    }
}

export default BaseModulePage;
