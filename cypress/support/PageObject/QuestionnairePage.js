import moment from "moment";

function selectAnswersInPlayerForDynamicQuestionnaire(questionDatas, dynamicQuestionnaireCount) {
    let count = dynamicQuestionnaireCount.questions; 
    let i = 1;
    questionDatas.forEach(QuestionDatas => {
        if (count >= i) {
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    cy.wait(3000);
                    cy.log("...............", attributeValue)
                    this.answerType(questionDatas, attributeValue)
                });
        }
        i++;
    });
}

class QuestionnairePage {


    click_new_questionnaire_button() {
        cy.wait(1000)
        cy.xpath("//*[@id='std-toolbar']/a",{timeout:50000}).should("be.visible").click()
    }

    enter_questionnaire_name(questionnaire,randomNumber) {
        cy.get('#questionnaire_name',{timeout:50000})
            .type(questionnaire["questionnaire_name"]+" "+randomNumber)
    }


    select_static_type_questionnaire() {
        cy.get(':nth-child(1) > .ui-checkboxradio-label',{timeout:50000}).should("be.visible").click()
    }

    select_dynamic_type_questionnaire() {
        cy.get(':nth-child(2) > .ui-checkboxradio-label',{timeout:50000}).should("be.visible").click()
    }


    click_create_button_on_questionnaire_create_page() {
        cy.get('#questionnaire_name-save-btn',{timeout:50000}).should("be.visible").click();
    }

    open_created_questionnaire(questionnaire_name) {
        cy.xpath("//a[contains(text(),'" + questionnaire_name["questionnaire_name"] + "')]",{timeout:50000}).should("be.visible").click()
    }

    click_selection_tab() {
        cy.xpath("//a[text()='Selection']",{timeout:50000}).should("be.visible")
            .click()
    }

    select_all_question() {
        cy.xpath("//div[@class='cell question-title']", {timeout: 50000}).should('be.visible')
        cy.scrollTo("top", {duration: 2000});
        cy.xpath("//a[@class='all-to-right-btn']", {timeout: 50000}).click()
    }

    click_save_button_on_selection_tab() {
        cy.xpath("//a[text()='Save']",{timeout:50000}).should("be.visible").click()
    }

    //To verify success message after added all questions into questionnaire
    verify_success_message_after_questionnaire_saved(questionnaire_name) {
        cy.xpath("//div[@class='xq-flashes xq-m collapse']//ul//li[normalize-space(text()='Your questionnaire " + questionnaire_name["questionnaire_name"] + " has been successfully saved')]", {timeout: 30000})
            .should('be.visible');
    }

    click_save_button_on_question_tab_after_enter_question_count() {
        cy.xpath("//button[text()='Save']",{timeout:50000}).should("be.visible").click()
    }

    click_question_tab_on_opened_questionnaire() {
        cy.xpath("//a[contains(@href,'/questionnaire/dynamic') and text()='Questions']",{timeout:50000}).should("be.visible").click()
    }

    enter_question_count_on_created_dynamic_questionnaire(dynamicQuestionnaireData) {
        cy.xpath("//div[@class='cell questionnaire_nb_questions']//input[@id='nb_questions']",{timeout:50000}).should("be.visible")
                .type(dynamicQuestionnaireData["questions"]);
    }

    click_trial_play_questionnaire_button(questionnaire) {
        cy.xpath("//a[contains(text(),'" + questionnaire["questionnaire_name"] + "')]//following::a[4]",{timeout:50000}).should("be.visible").click()
    }

    click_topic_filter_field() {
        cy.xpath("//*[@id='topic-filter-button']",{timeout:50000}).should("be.visible").click();
    }

    select_base_on_topic_filter_field(base) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + base + "']",{timeout:50000}).click();
    }

    select_all_topic_option(base) {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All topics')]",{timeout:50000}).click();
    }

    filter_created_base_on_questionnaire_list_page(baseName) {
        this.click_topic_filter_field()
        this.select_all_topic_option()
        this.click_topic_filter_field()
        this.select_base_on_topic_filter_field(baseName)

    }

    click_invitation_tab_on_open_questionnaire(base) {
        cy.xpath("//a[contains(@href,'/invite')]",{timeout:50000}).should("be.visible").click();
    }

    click_options_tab_on_opened_questionnaire() {
        cy.xpath("//a[contains(@href,'/option')]",{timeout:50000}).should("be.visible").click()
    }

    generate_random_string(string_length) {
        let random_number;
        for (let i = 0; i < string_length; i++) {
            random_number = Math.floor((Math.random() * 25) + 97);
            Cypress.env('Static_Random_number', random_number)
        }
        return random_number
    }

    enter_mail_subject(mail_sub, rand) {
        cy.xpath("//input[@id='subject']",{timeout:50000}).should("be.visible")
            .clear()
            .type(mail_sub["mail_subject"] + rand["rand_no"])
    }

    enter_mail_message(mail_sub) {
        cy.xpath("//div[@class='cell message ']//textarea[@id='message']",{timeout:50000}).should("be.visible")
                .clear()
                .type(mail_sub["mail_message"])
    }

    current_date() {
        let current_date_with_time
        const today_date = moment()

        const tdy_date_format = today_date.format('YYYY-MM-DD')
        cy.log(tdy_date_format)

        const nowTime = today_date.format('H:m')
        cy.log(nowTime)

        current_date_with_time = tdy_date_format + " " + nowTime;
        return current_date_with_time
    }


    tomorrow_date() {
        let tmr_date_with_time
        const tomorrow_date = moment().add(1, 'days');
        const tmr_date_format = tomorrow_date.format('YYYY-MM-DD')
        cy.log(tmr_date_format)

        const nowTime = tomorrow_date.format('H:m')
        cy.log(nowTime)

        tmr_date_with_time = tmr_date_format + " " + nowTime;
        return tmr_date_with_time
    }


    enter_start_date_as_current_date() {
        let current_date_with_time = this.current_date()
        cy.xpath("//input[@class='form_datetime form-control' and @id='start_date']",{timeout:50000}).should("be.visible")
            .clear()
            .type(current_date_with_time)
    }

    enter_end_date_as_tomorrow_date() {
        let tomorrow_date_with_time = this.tomorrow_date()
        cy.xpath("//input[@class='form_datetime form-control' and @id='end_date']",{timeout:50000}).should("be.visible")
                .clear()
                .type(tomorrow_date_with_time)
    }


    click_emails_and_mobile_button() {
        cy.get('#select-contacts-btn',{timeout:50000}).should("be.visible").click();
    }

    enter_active_user_mail_id(emails) {
        let email_id = emails["user_name"] + emails["domain"]
        cy.get('#contacts-participants',{timeout:50000}).should("be.visible")
            .type(email_id)
    }

    enter_contact_user_mail_id(emails) {
        cy.get('#contacts-participants',{timeout:50000}).should("be.visible")
            .type(emails["tets2020"]+emails["domain"])
    }

    click_save_button_on_emails_and_mobile_window() {
        cy.get('#save-contacts',{timeout:50000}).should("be.visible").click();
    }

    click_invite_button_on_questionnaire_invitation_page() {
        cy.xpath("//button[@id='invite']",{timeout:50000}).should("be.visible").click();
    }

    selectAnswersInPlayerForDynamicQuestionnaires(questionDatas, dynamicQuestionnaireCount) {
        let count = dynamicQuestionnaireCount.questions;
        cy.log("Count of dynamic outside loop", count)
        let i = 1;
        questionDatas.forEach(quesjson => {
            if (count >= i) {
                cy.log("Count of i", i)
                cy.log(quesjson)
                cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                    .invoke('attr', 'data-answer-type')
                    .then(attributeValue => {
                        cy.wait(3000);
                        cy.log("...............", attributeValue)
                        this.AllType(questionDatas, attributeValue)
                    });
            }
            i++;
        });
    }

    select_answers_in_static_player_by_contact_user(questiondatas) {
        questiondatas.forEach(question => {
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    // cy.wait(3000);
                    this.AllType(questiondatas, attributeValue)
                });
        });
        this.click_back_button_on_testscore_page_when_contactuser_played_in_mail()

    }

    select_answers_in_static_player_by_active_user(questiondatas) {
        questiondatas.forEach(question => {
            cy.log('my single type json has', question)
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    this.AllType(questiondatas, attributeValue)
                });
        });
        this.click_back_to_questionnaire_button_on_test_score_page()

    }

    select_answers_in_static_player_by_active_user_on_module_test(questiondatas) {
        questiondatas.forEach(question => {
            cy.log('my single type json has', question)
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    this.AllType(questiondatas, attributeValue)
                });
        });

    }

    select_answers_in_single_type_question(questiondatas) {
        questiondatas.forEach(question => {
            cy.log('my single type json has', question)
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    cy.log(attributeValue)
                    this.singleType(question, attributeValue)
                });
        });
        this.click_back_to_questionnaire_button_on_test_score_page()

    }

    select_answers_in_training_questionnaire_of_static_player_by_active_user(questiondatas) {
        questiondatas.forEach(question => {
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    // cy.wait(3000);
                    this.AllType(questiondatas, attributeValue)
                });
        });
        this.click_back_to_training_button_on_test_score_page()

    }

    select_answers_in_static_questionnaire_on_training_practice_by_active_user(questiondatas) {
        cy.url().should('include', '/windows/new')
        questiondatas.forEach(question => {
            cy.xpath("//div[@class='debrief' and(contains(@style,'none'))]/../div[@class='question ' and @data-answer-type]")
                .invoke('attr', 'data-answer-type')
                .then(attributeValue => {
                    // cy.wait(3000);
                    this.AllType(questiondatas, attributeValue)
                });
        });
        this.click_back_to_questionnaire_button_on_test_score_page()
    }


    verify_start_button_before_playing_static_questionnaire() {
        cy.wait(6000)
        cy.get('body').then((body) => {
            if (body.find('.slider-body').length > 0) {
                cy.get('.slider-body')
                    // cy.xpath("//div[@class='slider-body intro-page ui-front']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Start")) {
                            cy.xpath("//a[text()='Start']").click()
                        } else {
                            cy.log('the start button of intro page is not displayed')
                        }
                    });

            } else {
                cy.log('in start method')
            }
        });
    }

    verify_start_button_before_playing_dynamic_questionnaire(questiondata) {
        cy.wait(6000)
        cy.get('body').then((body) => {
            if (body.find('.slider-body').length > 0) {
                cy.xpath("//div[@class='slider-body intro-page ui-front']")
                    .then(function ($div) {
                        const text = $div.text()
                        cy.log("text we got is - " + text)
                        if (text.includes("Start")) {
                            cy.xpath("//a[text()='Start']").click()
                        } else {
                            selectAnswersInPlayerForDynamicQuestionnaire(questiondata)
                        }
                    });
            } else {
                selectAnswersInPlayerForDynamicQuestionnaire(questiondata)
            }
        });

    }

//To select answers at player
    AllType(questiondatas, attributeValue) {
        questiondatas.forEach(data => {
            cy.log("data", data)
            if (data.type === "single" && attributeValue === "mcq") {
                cy.log("It entered into single Type")
                cy.log("1", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']").click({multiple: true});
                    }
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()
            } else if (data.type === "multi" && attributeValue === "mchk") {
                cy.log("It entered into multi Type")
                cy.log("2", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']")
                            .click({force: true, multiple: true});
                    }
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "true_or_false" && attributeValue === "bool") {
                cy.log("It entered into true_or_false")
                cy.log("3", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p[text()='" + ans.answer_text + "']").click();
                    }
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "match_items" && attributeValue === "match") {
                cy.log("It entered into match_the_following")
                cy.log("4", data.type)
                let answer = data.answers;
                cy.wait(3000);

                let questionArray = [];
                let answerArray = [];
                cy.xpath("//div[@class='match-left']//p").each(val => {
                    questionArray.push(val.text())
                    cy.log("q..........", questionArray);
                    cy.log("Check question[0]", questionArray[0])
                    cy.log("Check question[1]", questionArray[1])

                    cy.xpath("//div[@class='sort-content']//p").each(val1 => {
                        answerArray.push(val1.text())
                        cy.log("a.............", answerArray);
                        cy.log("Check answer[0]", answerArray[0])
                        cy.log("Check answer[1]", answerArray[1])

                        if (questionArray[0] === answer[0].case) {
                            cy.log("it enter into 1st if condition")
                            if (answerArray[0] === answer[0].match_text) {
                                cy.log("it enter into 2nd if condition")
                                cy.log("It's already in correct order")
                            } else {
                                cy.log("it enter into else")
                                cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].match_text + "']")
                                    .trigger("mousedown", {which: 1})
                                cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].match_text + "']")
                                    .trigger("mousemove")
                                    .trigger("mouseup", )
                            }
                        }
                    })
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "sort_list" && attributeValue === "sort") {
                cy.log("it entered into sort_list")
                cy.log("5", data.type)
                let answer = data.answers;
                cy.wait(3000);

                cy.xpath("//div[@class='sortable ui-sortable']").then(val => {
                    let value = val.text();
                    cy.log("..........", value)
                    cy.log(answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text)

                    //here the wrap only for looping not for fetch data
                    cy.wrap(answer).each(data => {
                        cy.log("for loop", data)
                        if (value === answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text) {
                            cy.log("enters into if")
                        } else {
                            cy.log("enters into else")
                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(1)').first().trigger("mousemove", )
                                .trigger("mouseup", )

                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(2)').first().trigger("mousemove", )
                                .trigger("mouseup", )

                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[2].answer_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.get('.answer:nth-child(3)').first().trigger("mousemove", )
                                .trigger("mouseup", )
                        }
                    })
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "fill" && attributeValue === "fill") {
                cy.log("it entered into fill")
                cy.log("6", data.type)
                let Answer = data.answers;
                Answer.forEach(ans => {
                    if (ans.correct === true) {
                        cy.wait(3000);
                        cy.xpath("//div//p//input[@class='fill-blank']",{timeout:50000}).should("be.visible").type(ans.answer);
                    }
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "entered_answer" && attributeValue === "text") {
                cy.log("it entered into enter_answer")
                cy.log("7", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    cy.wait(3000);
                    cy.xpath("//div//input[@class='answer-given active']",{timeout:50000}).should("be.visible").type(ans.answer_text);
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "classify" && attributeValue === "classif") {
                cy.log("it entered into classification")
                cy.log("8", data.type)
                cy.wait(3000);
                cy.get('[data-name=_default] > div').then(questions => {
                    let lists = questions;
                    let i = 0;
                    cy.wrap(lists).each($elem => {
                        let value = $elem.text();
                        cy.wait(2000)
                        cy.xpath("//div[contains(@data-name,'answer_')]/div[contains(text(),'" + value + "')]/..",{timeout:50000}).should("be.visible").trigger("mousedown", {which: 1})
                        cy.get('[data-title=' + value + ']')
                            .trigger("mousemove")
                            .trigger("mouseup", )
                        cy.wait(2000)
                        i++
                    })
                })

            } else if (data.type === "Select" && attributeValue === "fsel") {
                cy.log("it entered into select_type")
                cy.log("9", data.type)
                let answer = data.answers;
                answer.forEach(ans => {
                    cy.wait(3000);
                    cy.xpath("(//span[@class='ui-selectmenu-text'])[1]",{timeout:50000}).should("be.visible").click();
                    cy.xpath("//li[@class='ui-menu-item']//div[text() = '" + ans.correct_answer + "']",{timeout:50000}).should("be.visible").click();
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "softsingle" && attributeValue === "heart") {
                cy.log("10", data.type)
                cy.log("it entered into soft_skill_single")
                cy.wait(3000);
                cy.xpath("(//div[@class='answer answer-choice active'])[1]",{timeout:50000}).should("be.visible").click();
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            } else if (data.type === "softmultiple" && attributeValue === "heart_multiple") {
                // cy.wait(9000);
                cy.log("11", data.type)
                cy.log("it entered into soft_skill_multiple")
                let answer = data.answers;
                answer.forEach(ans => {
                    let index = 1
                    // cy.wait(9000);
                    if (ans.correct === true) {
                        cy.xpath("(//div[@class='answer answer-multi-choice active'])[" + index + "]",{timeout:50000}).should("be.visible").click();
                    }
                })
                // this.click_validate_and_move_next_question_in_questionnaire_player();
                // this.see_test_score_page()

            }
        });
        this.click_validate_and_move_next_question_in_questionnaire_player();
        this.see_test_score_page()
    }

    singleType(data, attributeValue) {
        cy.log("Entered into main loop", data.type)
        // questiondatas.forEach(data => {
        //     cy.log("data", data)
        if (data.type === "single" && attributeValue === "mcq") {
            cy.log("1", data.type)
            let answer = data.answers;
            answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p[text()='" + ans.answer_text + "']",{timeout:50000}).should("be.visible").click({multiple: true});
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()
        } else if (data.type === "multi" && attributeValue === "mchk") {
            cy.log("2", data.type)
            let answer = data.answers;
            answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p[text()='" + ans.answer_text + "']",{timeout:50000}).should("be.visible")
                        .click({force: true, multiple: true});
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "true_or_false" && attributeValue === "bool") {
            cy.log("3", data.type)
            let answer = data.answers;
            answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p[text()='" + ans.answer_text + "']",{timeout:50000}).should("be.visible").click();
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "match_items" && attributeValue === "match") {
            cy.log("4", data.type)
            let answer = data.answers;
            cy.wait(3000);
            let questionArray = [];
            let answerArray = [];
            cy.xpath("//div[@class='match-left']//p").each(val => {
                questionArray.push(val.text())
                cy.log("q..........", questionArray);
                cy.log("Check question[0]", questionArray[0])
                cy.log("Check question[1]", questionArray[1])
                cy.xpath("//div[@class='sort-content']//p").each(val1 => {
                    answerArray.push(val1.text())
                    cy.log("a.............", answerArray);
                    cy.log("Check answer[0]", answerArray[0])
                    cy.log("Check answer[1]", answerArray[1])

                    if (questionArray[0] === answer[0].case) {
                        cy.log("it enter into 1st if condition")
                        if (answerArray[0] === answer[0].match_text) {
                            cy.log("it enter into 2nd if condition")
                            cy.log("It's already in correct order")
                        } else {
                            cy.log("it enter into else")
                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].match_text + "']")
                                .trigger("mousedown", {which: 1})
                            cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].match_text + "']")
                                .trigger("mousemove")
                                .trigger("mouseup", )
                        }
                    }
                })
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "sort_list" && attributeValue === "sort") {
            cy.log("it entered into sort_list")
            cy.log("5", data.type)
            let answer = data.answers;
            cy.wait(3000);

            cy.xpath("//div[@class='sortable ui-sortable']").then(val => {
                let value = val.text();
                cy.log("..........", value)
                cy.log(answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text)

                //here the wrap only for looping not for fetch data
                cy.wrap(answer).each(data => {
                    cy.log("for loop", data)
                    if (value === answer[0].answer_text + " " + answer[1].answer_text + " " + answer[2].answer_text) {
                        cy.log("enters into if")
                    } else {
                        cy.log("enters into else")
                        cy.xpath("//div[@class='sort-content']//p[text()='" + answer[0].answer_text + "']")
                            .trigger("mousedown", {which: 1})
                        cy.get('.answer:nth-child(1)').first().trigger("mousemove", )
                            .trigger("mouseup", )

                        cy.xpath("//div[@class='sort-content']//p[text()='" + answer[1].answer_text + "']")
                            .trigger("mousedown", {which: 1})
                        cy.get('.answer:nth-child(2)').first().trigger("mousemove", )
                            .trigger("mouseup", )

                        cy.xpath("//div[@class='sort-content']//p[text()='" + answer[2].answer_text + "']")
                            .trigger("mousedown", {which: 1})
                        cy.get('.answer:nth-child(3)').first().trigger("mousemove", )
                            .trigger("mouseup", )
                    }
                })
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "fill" && attributeValue === "fill") {
            cy.log("it entered into fill")
            cy.log("6", data.type)
            let Answer = data.answers;
            Answer.forEach(ans => {
                if (ans.correct === true) {
                    cy.wait(3000);
                    cy.xpath("//div//p//input[@class='fill-blank']",{timeout:50000}).should("be.visible").type(ans.answer);
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "entered_answer" && attributeValue === "text") {
            cy.log("it entered into enter_answer")
            cy.log("7", data.type)
            let answer = data.answers;
            answer.forEach(ans => {
                cy.wait(3000);
                cy.xpath("//div//input[@class='answer-given active']",{timeout:50000}).should("be.visible").type(ans.answer_text);
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "classify" && attributeValue === "classif") {
            cy.log("it entered into classification")
            cy.log("8", data.type)
            cy.wait(3000);
            cy.get('[data-name=_default] > div').then(questions => {
                let lists = questions;
                let i = 0;
                cy.wrap(lists).each($elem => {
                    let value = $elem.text();
                    cy.wait(2000)
                    cy.xpath("//div[contains(@data-name,'answer_')]/div[contains(text(),'" + value + "')]/..",{timeout:50000}).should("be.visible").trigger("mousedown", {which: 1})
                    cy.get('[data-title=' + value + ']')
                        .trigger("mousemove")
                        .trigger("mouseup", )
                    cy.wait(2000)
                    i++
                })
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()
        } else if (data.type === "Select" && attributeValue === "fsel") {
            cy.log("it entered into select_type")
            cy.log("9", data.type)
            let answer = data.answers;
            answer.forEach(ans => {
                cy.wait(3000);
                cy.xpath("(//span[@class='ui-selectmenu-text'])[1]",{timeout:50000}).should("be.visible").click();
                cy.xpath("//li[@class='ui-menu-item']//div[text() = '" + ans.correct_answer + "']").click();
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "softsingle" && attributeValue === "heart") {
            cy.log("10", data.type)
            cy.log("it entered into soft_skill_single")
            cy.wait(3000);
            cy.xpath("(//div[@class='answer answer-choice active'])[1]",{timeout:50000}).should("be.visible").click();
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        } else if (data.type === "softmultiple" && attributeValue === "heart_multiple") {
            // cy.wait(9000);
            cy.log("11", data.type)
            cy.log("it entered into soft_skill_multiple")
            let answer = data.answers;
            answer.forEach(ans => {
                let index = 1
                // cy.wait(9000);
                if (ans.correct === true) {
                    cy.xpath("(//div[@class='answer answer-multi-choice active'])[" + index + "]",{timeout:50000}).should("be.visible").click();
                }
            })
            // this.click_validate_and_move_next_question_in_questionnaire_player();
            // this.see_test_score_page()

        }
        this.click_validate_and_move_next_question_in_questionnaire_player();
        this.see_test_score_page()
    }

    //To click validate and next buttons
    click_validate_and_move_next_question_in_questionnaire_player() {
        cy.xpath("//div//a//i[@class='fa fa-check']",{timeout:50000}).should("be.visible").click({force: true, multiple: true});
    }

    see_test_score_page() {
        cy.get('body').then((body) => {
            if (body.find("a[title='Next Question']").length > 0) {
                cy.get("a[id='next-btn'][title]")
                    .invoke('attr', 'title')
                    .then(attibuteValue => {
                        const text = attibuteValue
                        cy.log("text we got is - " + text)
                        if (text.includes("Next Question")) {
                            // cy.xpath("//a[@title='See Test Score']")
                            //     .click()
                            cy.xpath("//a[@title='Next Question']",{timeout:50000}).should("be.visible")
                                .click()
                        } else {
                            cy.xpath("//a[@title='See Test Score']",{timeout:50000}).should("be.visible")
                                .click()
                            // cy.xpath("//a[@title='Next Question']")
                            //     .click()
                        }
                    });
            } else {
                cy.xpath("//a[@title='See Test Score']",{timeout:50000}).should("be.visible")
                    .click()
            }
        });
    }


    Checking_next_qu_and_ex_button(attributevalue) {
        cy.log(attributevalue)
        if (attributevalue.includes("Next Question")) {
            if (attributevalue.should('be.visible')) {
                cy.xpath("//a[@title='See Test Score']",{timeout:50000}).should("be.visible").click();
            }
            else {
                cy.xpath("//a[@title='Next Question']",{timeout:50000}).should("be.visible").click();
            }
        }
    }

    click_result_tab_on_questionnaire_result_page() {
        cy.xpath("//a[text()='Results']",{timeout:50000}).should("be.visible")
            .click()
    }

    click_per_evaluation_tab_on_questionnaire_result_page() {
        cy.xpath("//a[text()='Per evaluation']",{timeout:50000}).should("be.visible")
            .click()
    }

    click_free_and_public_tests_tab_on_questionnaire_result_page() {
        cy.xpath("//a[text()='Free and public tests']",{timeout:50000}).should("be.visible")
            .click()
    }


    verify_questionnaire_result_for_active(questionnaire, userdata) {
        cy.wrap(userdata).each(user => {
            let user_count = Cypress.$(user).length
            cy.log(user_count)
            cy.xpath("//a[contains(text(),'" + questionnaire["questionnaire_name"] + "')]/../..//strong[text()='" + user_count + "/1']",{timeout:50000}).should("be.visible")
                .click()
        })
    }

    verify_questionnaire_result_for_contact(questionnaire, contact) {
        cy.wrap(contact).each(user => {
            let user_count = Cypress.$(user).length
            cy.log(user_count)
            cy.xpath("//a[contains(text(),'" + questionnaire["questionnaire_name"] + "')]/../..//strong[text()='" + user_count + "/1']",{timeout:50000}).should("be.visible")
                .click()
        })
    }

    verify_public_result_for_guest_user(questionnaire) {
        cy.xpath("//div[normalize-space(text())='Guest']/..//div[normalize-space(text())='" + questionnaire["questionnaire_name"] + "']",{timeout:50000}).should("be.visible")
            .click()
    }

    verify_visibility_result_for_active_user(questionnaire, userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//div[normalize-space(text())='" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "']/..//div[normalize-space(text())='" + questionnaire["questionnaire_name"] + "']",{timeout:50000}).should("be.visible")
                .click()
        })
    }

    click_select_user_button_on_questionnaire_invitation_page() {
        cy.xpath("//a[@id='select-employees-btn']",{timeout:50000}).should("be.visible")
            .click()
    }

    select_activated_user_on_questionnaire_invitation_page(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//span[contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "')]/../..//div[@class='cell btns']",{timeout:50000}).should("be.visible")
                .click()
        })
    }

    click_validate_buttton_on_questionnaire_invitation_page() {
        cy.xpath("//a[@class='btn btn-primary save-btn']",{timeout:50000}).should("be.visible")
            .click()
    }

    click_the_visibility_tab_on_opened_questionnaire() {
        cy.xpath("//a[text()='Visibility']",{timeout:50000}).should("be.visible")
            .click()
    }

    click_the_make_it_public_button() {
        cy.xpath("//a[text()='Make it public']",{timeout:50000}).should("be.visible")
            .click()
    }

    enter_public_link(questionnaire) {
        // questionnaire.forEach(data => {
        let public_link = questionnaire["link_text"];
        cy.xpath("//input[@id='public_link']",{timeout:50000}).should("be.visible")
            .type(public_link)
    }

    enter_return_link(link) {
        cy.xpath("//input[@id='public_back_url']",{timeout:50000}).should("be.visible")
            .type(link["redirect_url"])
    }

    make_public_button_on_create_public_popup() {
        cy.xpath("//button[text()='Make Public']",{timeout:50000}).should("be.visible")
            .click()
    }

    get_the_public_link_using_the_QR_code() {
        cy.xpath("(//div[@class='flex-row public-link']//a[text()])[1]",{timeout:50000})
            .then(function ($div) {
                const text = $div.text()
                cy.log("we got is public link is - " + text)
                Cypress.env("public link", text)
                cy.log(Cypress.env("public link"))
                cy.readFile("cypress/fixtures/user_reg_link.json", (err, link) => {
                    if (err) {
                        return cy.log("error");
                    }
                }).then((link) => {
                    link.public_link = text
                    cy.writeFile("cypress/fixtures/user_reg_link.json", JSON.stringify(link))
                })
            })

    }

    created_group_checked(group) {
        cy.xpath("//label[contains(@for,'group_key')]",{timeout:50000})
            .then(function ($div) {
                const text = $div.text()
                cy.log("we got the created group name on questionnaire visibility - ", text)
                let json_group_name = group["group-1"]
                cy.xpath("//div[@id='groups-list']//label[contains(.,'" + json_group_name + "')]").click()
            })
    }

    click_save_button_on_questionnaire_visibility_page() {
        cy.xpath("(//button[@class='btn btn-primary'])[1]",{timeout:50000}).should("be.visible")
            .click()
    }

    click_before_survey_form_filter_field_on_questionnaire_option_page() {
        cy.xpath("//span[@id='survey_form_select-button']",{timeout:50000}).should("be.visible").click()
    }

    click_after_survey_form_filter_field_on_questionnaire_option_page() {
        cy.xpath("//span[@id='survey_form_select_2-button']",{timeout:50000}).should("be.visible").click()
    }

    select_form_on_questionnaire_option_page(form) {
        this.click_before_survey_form_filter_field_on_questionnaire_option_page()
        cy.xpath("//*[@id='survey_form_select-menu']/li/div[contains(text(),'" + form["form_name"] + "')]",{timeout:50000}).should("be.visible").click()
        this.click_after_survey_form_filter_field_on_questionnaire_option_page()
        cy.xpath("//*[@id='survey_form_select_2-menu']/li/div[contains(text(),'" + form["form_name"] + "')]",{timeout:50000}).should("be.visible").click()
    }

    click_save_on_questionnaire_option_page() {
        cy.xpath("(//button[@class='btn btn-primary'])[1]",{timeout:50000}).should("be.visible").click()
    }

    click_back_button_on_testscore_page_when_contactuser_played_in_mail() {
        cy.xpath("//a[@class='btn btn-primary']",{timeout:50000}).should("be.visible").click()
    }

    click_back_to_dashboard_button_on_testscore_page_when_active_user_played_in_her_account() {
        cy.xpath("//a[text()='Back to dashboard']",{timeout:50000}).should("be.visible").click()
    }

    click_back_to_questionnaire_button_on_test_score_page() {
        cy.xpath("//a[@class='btn btn-primary']",{timeout:50000}).should("be.visible")
            .click()
    }

    click_back_to_training_button_on_test_score_page() {
        cy.xpath("//a[text()='Back to training']",{timeout:50000}).should("be.visible")
            .click()
    }


}

export default QuestionnairePage;