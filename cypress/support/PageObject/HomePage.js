function delete_if_admin_account_is_exist(common_data) {
    let delete_admin_account = common_data["delete_www_account"]
    cy.visit(delete_admin_account)
}

class HomePage {

    click_register_button() {
        // return cy.xpath("//a[contains(.,'Register')]").click()
        cy.xpath("(//a[contains(.,'Free trial')])[1]",{timeout:50000}).should("be.visible")
            .click()
    }

    enter_email_id_on_registeration_page(email) {
        let email_id;
        email_id = email["last_name"] + email["first_name"] + email["domain"];
        return cy.xpath("//*[@id='register-form']//*[@name='email_or_mobile']",{timeout:50000}).should("be.visible").type(email_id)
    }

    enter_first_name_on_registeration_page(name) {
        cy.get('#first_name',{timeout:50000}).should("be.visible").type(name["first_name"])
    }

    enter_last_name_on_registeration_page(name) {
        cy.xpath("//*[@id='last_name']",{timeout:50000}).should("be.visible").type(name["last_name"])
    }

    enter_password_on_registeration_page(password) {
        cy.xpath("//form[@id='register-form']//input[@name='password']",{timeout:50000}).should("be.visible").type(password["password"])
    }

    enter_confirm_password_on_registeration_page(password) {
        cy.xpath("//form[@id='register-form']//input[@name='password2']",{timeout:50000}).should("be.visible").type(password["password"])

    }

    enter_enterprise_name_on_registeration_page(enterprise_name) {
        cy.xpath("//input[@name='enterprise_name']",{timeout:50000}).should("be.visible")
            .type(enterprise_name["enterprise"])

    }

    click_register_button_on_registeration_page() {
        cy.xpath("//button[text()='Register']",{timeout:50000}).should("be.visible")
            .click();
    }

    click_connection_button() {
        cy.get("#main-nav > :nth-child(7) > a", { timeout: 50000 }).should("be.visible").click()
    }

    enter_admin_email_id(email) {
        let email_id;
        email_id = email["last_name"] + email["first_name"] + email["domain"];
        return cy.get("#sign-in-form input[name='email_or_mobile']", { timeout: 50000 }).should("be.visible").type(email_id)
    }

    enter_admin_password(passwords) {
        cy.get("#sign-in-form input[name='password']",{timeout:50000}).should("be.visible").type(passwords["password"])
    }

    click_sign_in_button() {
        cy.get('#sign-in-form > .btn-toolbar > .btn-primary',{timeout:50000}).should("be.visible").click()
        cy.wait(5000)
    }

    log_out_admin() {
        cy.xpath("//div[@class='login-slide']",{timeout:50000})
            .should('be.hidden')
            .invoke('show')
        cy.xpath("//div[@class='login-slide']//li//a[text()='Log Out']",{timeout:50000}).should("be.visible")
            .click({ force: true })
    }

    verifyRegistered(admin_user) {
        cy.xpath("//div/strong/a[contains(text(),'" + admin_user['enterprise'] + "')]",{ timeout: 10000 }).should("be.visible")
    }

    verify_and_delete_registered_admin_if_logged(common_data) {
        cy.wait(6000)
        cy.get('body').then((body) => {
            if (body.find("#sign-in-form > .error-message",{timeout:50000}).length > 0) {
                cy.get('#sign-in-form > .error-message',{timeout:50000})
                    .then(function ($div) {
                        const text = $div.text()
                        if (text.includes("Invalid email or password")) {
                            // if (text.includes("Enterprise is erroneous")) {
                            cy.xpath("//div[@id='reset-password']//div[@class='footer']/p/a[contains(text(),'return to home page')]",{timeout:50000})
                            .should("be.visible").click()
                        } else {
                            delete_if_admin_account_is_exist(common_data)
                        }
                    });
            } else {
                delete_if_admin_account_is_exist(common_data)
            }
        });
    }

    delete_if_admin_account_is_exist(link) {
        let delete_admin_account = link["delete_www_account"]
        cy.visit(delete_admin_account)
    }

    verify_error_message_for_unregistered_account(common_data) {
        cy.get('body').then((body) => {
            if (body.find("#sign-in-form > .error-message",{timeout:50000}).length > 0) {
                cy.get('#sign-in-form > .error-message',{timeout:50000}).should("be.visible")
            } else {
                delete_if_admin_account_is_exist(common_data)
            }
        })
    }
      //Register Without Mandatory Field//

      clickFreeTrialButton() {
        cy.get('#main-nav>li.highlight.blue>a').click();
    }
    enterPasword(requirefieldtest) {
        cy.xpath("//form[@id='register-form']/..//input[@name='password']").should('be.visible')
        cy.xpath("//form[@id='register-form']/..//input[@name='password']")
            .type(requirefieldtest["Password"])

    }
    enterConfirmPasword(requirefieldtest) {
        cy.xpath("//form[@id='register-form']/..//input[@name='password2']")
            .type(requirefieldtest["ConfirmPassword"])
            .log("Successfully entered the Confirm password");
    }
    clickRegisterButtononRegisterationPage() {
        cy.get('#register-form > div > button').click();
    }
    verifyWithoutEnteringTheMandatoryField() {
        cy.xpath("//input[@name='email_or_mobile']/..//p[@class='field-error']")
            .should('contain', 'This field is required')
        cy.xpath("//input[@name='first_name']/..//p[@class='field-error']")
            .should('contain', 'This field is required')
        cy.xpath("//input[@name='last_name']/..//p[@class='field-error']")
            .should('contain', 'This field is required')
        // cy.xpath("//input[@name='password']/..//p[@class='field-error']", { timeout: 10000 })
        // .should('contain', 'This field is required')
        // cy.xpath("//input[@name='password2']/..//p[@class='field-error']", { timeout: 10000 })
        // .should('contain', 'This field is required')
        cy.xpath("//input[@name='enterprise_name']/..//p[@class='field-error']")
            .should('contain', 'This field is required')
    }

    // Verify the Exist EmailID on Registration page

    enterEmailMobileNumber(requirefieldtest) {
        cy.xpath("//form[@id='register-form']//input[@name='email_or_mobile']")
            .type(requirefieldtest["EmailID"])
    }
    enterFstName(requirefieldtest) {
        cy.xpath("//form[@id='register-form']//input[@name='first_name']")
            .type(requirefieldtest["FirstName"])
    }
    enterLstName(requirefieldtest) {
        cy.xpath("//form[@id='register-form']//input[@name='last_name']")
            .type(requirefieldtest["LastName"])
    }
    verifyExistEmailField(requirefieldtest) {
        cy.xpath("//p[@class='field-error']")
        .should('contain', 'This email is already registered.')
    }

    // Verify the Character limit on Registration page

    enterEmailorMobileNumber(characterlimit) {
        cy.xpath("//form[@id='register-form']//input[@name='email_or_mobile']")
            .type(characterlimit["EmailID"])
    }
    enterFirstName(characterlimit) {
        cy.xpath("//form[@id='register-form']//input[@name='first_name']")
            .type(characterlimit["FirstName"])
    }
    enterLastName(characterlimit) {
        cy.xpath("//form[@id='register-form']//input[@name='last_name']")
            .type(characterlimit["LastName"])
    }
    enterPassword(characterlimit) {
        cy.xpath("//form[@id='register-form']//input[@name='password']")
            .type(characterlimit["Password"])

    }
    enterConfirmPassword(characterlimit) {
        cy.xpath("//form[@id='register-form']//input[@name='password2']")
            .type(characterlimit["ConfirmPassword"])

    }
    enterEnterpriseName(characterlimit) {
        cy.get('#enterprise_name').type(characterlimit["EnterpriseName"])

    }
    clickRegisterButtononRegisterationPage() {
        cy.get('#register-form > div > button').click();
    }
    mailAlertMessageForCharacterLimit(characterlimit) {
        cy.xpath("//input[@name='email_or_mobile']/..//p[@class='field-error']")
            .should('contain', 'Field must be less than 60 characters long.')
    }

    firstAndLastNameAlertMessageForCharacterLimit(characterlimit) {
        cy.xpath("//input[@name='first_name']/..//p[@class='field-error']")
            .should('contain', 'Field must be between 1 and 30 characters long.')
        cy.xpath("//input[@name='last_name']/..//p[@class='field-error']")
            .should('contain', 'Field must be between 1 and 30 characters long.')
    }
    // passwordandConfirmPasswordAlertMessageForCharacterLimit(characterlimit) {
    //     cy.xpath("//input[@name='password']/../..//p[@class='field-error']")
    //     .should('contain', 'Field must be between 1 and 30 characters long.')
    //     cy.xpath("//input[@name='password2']/../..//p[@class='field-error']")
    //     .should('contain', 'Field must be between 1 and 30 characters long.')
    // }
    enterpriseAlertMessageForCharacterLimit(characterlimit) {
        cy.xpath("//input[@name='enterprise_name']/..//p[@class='field-error']")
            .should('contain', 'Field must be between 1 and 60 characters long.')
    }

    // Verify the Password and confirm password mismatch

    enterEmailID(invalidConfirmpassword) {
        cy.xpath("//form[@id='register-form']//input[@name='email_or_mobile']")
            .type(invalidConfirmpassword["EmailID"])
    }
    enterInvalidconfirmPassword(invalidConfirmpassword) {
        cy.xpath("//form[@id='register-form']//input[@name='password2']")
            .type(invalidConfirmpassword["confirmPassword"]);
    }
    verifyThepasswordMismatcherror() {
        cy.xpath("//input[@name='password']/../..//p[@class='field-error']")
            .should('contain', 'Passwords must match.')
    }
    // Registration from signin popup

    clickRegisterlinkFromsigninPopup() {
        cy.xpath("//a[@href='#register']").click();
    }
    verifyregisterPopup() {
        cy.xpath("//form[@id='register-form']").should("be.visible")
    }



}

export default HomePage;