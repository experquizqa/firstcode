export default class Samplepage {

  visit(url) {
    cy.visit(url);
  }

  click(selector, field) {
    return cy.get(selector,{timeout:50000}).click()
  }
  type(selector, text, field) {
    try {
      return cy.get(selector,{timeout:50000}).type(text)
    } catch (error) {
      return cy.log("Fail");
    }
  }
  getAttribute(selector, attributename, field) {
    let attr;
    try {
      return cy.get(selector,{timeout:50000}).then(function (el) {
        attr = el.prop(attributename);
      });
    } catch (error) {}
  }
  selectByText(selector, text, field) {
    try {
      return cy.get(selector,{timeout:50000}).select(text)
    } catch (error) {
      return cy.log("Fail");
    }
  }
  getText(selector, field) {
    let text;
    try {
      return cy.get(selector,{timeout:50000}).then(function (el) {
        text = el.text();
      });
    } catch (error) {}
  }
  windowsalert(text){
   return cy.on("window:alert", str => {
        expect(str).equal(text);
      });
  }
  windowsAlertConfirm(text){
    return cy.on("window:confirm", str => {
         expect(str).equal(text);
       });
   }
}
