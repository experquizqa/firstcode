class MediaPage {

    click_media_side_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Medias").click()
    }

    click_create_media_button() {
        cy.get("a#open-btn", {timeout: 50000}).should('be.visible').click()
    }

    click_create_zip_button() {
        cy.get("a#zip-btn", {timeout: 50000}).should('be.visible').click()
    }

    click_upload_a_file_button(media) {
        if (media["type"] === "pdf" || media["type"] === "docx" || media["type"] === "image" || media["type"] === "xlsx") {
            const filepath = media["name"]
            const fileName = media["name"]
            cy.fixture(filepath).then((fileContent) => {
                cy.get('input[name="upl"]')
                    .wait(3000)
                    .attachFile({
                            fileContent, fileName, mimeType: 'docx/pdf/png/xlsx',
                        },
                        {
                            subjectType: 'drag-n-drop'
                        },
                    )
                cy.wait(3000)
            })
        } else {
            cy.xpath("//a[@id='cancel-btn']", {timeout: 50000}).should('be.visible').click()
        }
    }


    verify_uploaded_media_is_exist(media) {
        cy.xpath("//div[@class='filename']//span[contains(text(),'" + media + "')]", {timeout: 50000}).should('be.visible')
    }

    click_factsheet_button() {
        cy.wait(5000)
        cy.get("a#factsheet-btn", {timeout: 50000}).should("be.visible").click({force:true})
    }

    enter_factsheet_title(media_name) {
        cy.get('#factsheet-edit-modal > .modal-dialog > .modal-content > .modal-body > #factsheet-form > :nth-child(5) > #title', {timeout: 50000}).should('exist')
            .type(media_name)
    }

    enter_factsheet_contents(factsheet_content) {

        cy.xpath("//iframe[@class='cke_wysiwyg_frame cke_reset']").then($element => {
            const $body = $element.contents().find('body')
            let stripe = cy.wrap($body)
            stripe.find('p', {timeout: 50000}).eq(0).should('be.visible').click().type(factsheet_content)
        })

    }


    save_factsheet_sheet() {
        cy.xpath("//button[@title='Save Sheet']", {timeout: 50000}).scrollIntoView()
            .click()
        cy.wait(1000)
    }

    click_embed_button() {
        cy.xpath("//*[@id='embedded-video-btn']", {timeout: 50000})
            .click()
    }

    enter_embed_title(media_name) {
        cy.xpath("//form[@id='embedded-video-form']//input[@id='title']", {timeout: 50000})
            .type(media_name)
    }

    enter_embed_code(embed_code) {
        cy.xpath("//*[@id='html']", {timeout: 50000}).should('be.visible')
            .type(embed_code)
    }

    save_embed_video() {
        cy.xpath("//*[@title='Save Embedded Video']", {timeout: 50000}).scrollIntoView()
            .click()
    }

}

export default MediaPage;