import * as path from "path";

class BasePage {

    click_admin_base_menu() {
        cy.reload()
        cy.wait(5000)
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Bases").click()
    }

    click_new_base_button() {
        cy.get('#new-base-btn', {timeout: 50000}).should("be.visible").click();
    }

    enter_base_name(baseName) {
        cy.get('#topic_text', {timeout: 50000}).type(baseName);
    }

    enter_domain_name(domains) {
        let domain_count = 0;
        cy.wrap(domains).each(domain => {
            cy.xpath("//input[@id='domains-" + domain_count + "-domain_text']", {timeout: 50000}).type(domain);
            // cy.xpath("//a[contains(@class,'add-domain')]", {timeout: 50000}).first().click();
            domain_count = domain_count + 1;
        });
    }

    clearEnteredDomain() {
        cy.get('#domains > div.bloc-body > div:nth-child(2) > div.cell.domain-text > div', {timeout: 50000}).clear()
    }


    save_create_base_page() {
        cy.get('#standard-toolbar > .btn-primary', {timeout: 50000}).should("be.visible").click();

    }

    verify_created_base_exist(baseName) {
        cy.xpath("//div[@class='flex-row']//a[contains(text(),'" + baseName + "')]", {timeout: 50000}).should("exist");
           
    }

    open_created_base(baseName) {
        cy.xpath("(//div[@class='flex-row']//a[contains(text(),'" + baseName + "')])[1]", {timeout: 50000}).should("be.visible").click();
    }

    click_questions_tab() {
        cy.get("div ul.nav-tabs-list.list-unstyled li a", {timeout: 50000}).contains("Questions").scrollIntoView().should("be.visible").click();
    }

    click_new_question_button() {
        cy.xpath("//a[text()='New Question']", {timeout: 50000}).should("be.visible").click();
    }

    click_questionnaire_tab() {
        cy.get("div ul.nav-tabs-list.list-unstyled li a", {timeout: 50000}).contains("Questionnaires").should("be.visible").click()

    }

    click_new_questionnaire_button() {
        cy.wait(1000)
        cy.xpath("//*[@id='std-toolbar']/a", {timeout: 50000}).should("be.visible").click()

    }

    click_create_button() {
        cy.get('#questionnaire_name-save-btn', {timeout: 50000}).should("be.visible").click();

    }

    click_import_and_export_tab() {
        cy.get("ul.nav-tabs-list li a", {timeout: 50000}).contains('Import & Export').scrollIntoView().should("be.visible").click()
        cy.wait(1000)
    }

    click_upload_file_button() {
        // const downloadsFolder = Cypress.config("downloadsFolder");
        let filename = "../downloads/experquiz-howto-upload-v7-en.docx"
        cy.get('input[type="file"]', {timeout: 50000})
            .attachFile(filename)
        cy.reload()
    }

    click_to_download_the_sample_question_file() {
        cy.window().document().then(function (doc) {
            doc.addEventListener('click', () => {
                setTimeout(function () {
                    doc.location.reload()
                }, 5000)
            })
            cy.xpath("//a[@class='btn btn-primary download-btn ']", {timeout: 50000})
                .should("be.visible").click()
        })

    }

    verify_file_is_downloaded() {
        const downloadsFolder = Cypress.config("downloadsFolder");
        cy.readFile(path.join(downloadsFolder, "experquiz-howto-upload-v7-en.docx"), {timeout: 50000})
            .should("exist");
    }

    reload_import_page() {
        cy.get('body')
            .then(($file) => {
                const FilePanelIsVisible = $file.find('[id=mcq-files-list]')
                    .is(':visible');
                if (FilePanelIsVisible) {
                    this.CheckImportedFileStatus()
                } else {
                    cy.reload();
                    cy.wait(10);
                    this.reload_import_page()
                }

            })
    }

    CheckImportedFileStatus = () => {
        cy.get('body').then($mainContainer => {
            const isVisible = $mainContainer.find("div[class='flex-row file-div'] i[class='fa fa-check']")
                .is(':visible');
            if (isVisible) {
                cy.xpath("//div[@class='big-btn ']//span[text()='imported']/../strong")
                    .then(function ($count) {
                        let get_imported_count = $count.text()
                        cy.wrap(get_imported_count).as('import_question_count')
                    })
            } else {
                cy.reload()
                this.CheckImportedFileStatus();
            }
        });
    }


    verify_imported_question_is_in_base_of_question_page() {
        this.click_questions_tab()
        cy.get('@import_question_count').then(count => {
            cy.wait(10000)
            cy.xpath("//h2[@class='message' and contains(text(),'" + count + "')]",{timeout:30000})
        })
    }

    click_admin_user_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Users").click({waitForAnimations: false});
    }

    click_dashaboard_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Dashboard").click();
    }

    click_media_tab_on_base() {
        cy.xpath("//a[text()='Medias']", {timeout: 50000}).should("be.visible").click()
    }

    enterTag(tagname) {
        cy.get('#display-outer_tags_list>div>ul>li>div', {timeout: 50000}).type(tagname)
    }

    //Search using tagfield
    entertagfield(tagname) {
        cy.get('div.tag-editor-wrapper>ul', {timeout: 50000}).should('be.visible').type(tagname)
            .type('{enter}')
    }

    verifyTheBase(base) {
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + base + "')]", {timeout: 50000}).should('be.visible')
    }

    selectTheBase(base) {
        cy.xpath("//a[contains(text(),'" + base + "')]/..//span[@class='fa fa-square-o unchecked  ']", {timeout: 50000}).should("be.visible").click()
    }

    clickArchiveBtn() {
        cy.get('#topics-archive-btn', {timeout: 50000}).should("be.visible").click()
    }

    verify_the_archived_base_not_displayed(base) {
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + base["base_name"] + "')]", {timeout: 50000})
            .should('not.be.visible')
    }

    chooseStatusFilter() {
        cy.get('#status-filter-button', {timeout: 50000}).should("be.visible").click()
    }

    selectAllStatus() {
        cy.xpath("//div[text()='All status']", {timeout: 50000}).should("be.visible").click()
    }

    verifyAllCountOfArchivedAndActiveBases() {
        let countbases
        let totalcount
        var list = []
        cy.get('div.topic-div').then(count => {
            countbases = count.length;
            totalcount = countbases.toString().slice('')
        })

        cy.get('span.tts-auto').invoke('text').then((text) => {
            var fullText = text;
            var pattern = /[0-9]+/g;
            var number = fullText.match(pattern);
            expect(number[0]).to.equal(totalcount);
        })
    }

    selectArchiveOption() {
        cy.xpath("//div[text()='Archived']", {timeout: 50000}).should("be.visible").click()
    }

    verifyTheArchivedBase(base) {
        cy.xpath("//a[@class='cell topic-name archived']", {timeout: 50000}).then(($ele) => {
            const text = $ele.text()
            expect(text).to.contains(base)
        })
    }

    selectTheArchivedBase(base) {
        cy.xpath("//a[contains(text(),'" + base + "')]/..//span[@class='fa fa-square-o unchecked  ']", {timeout: 50000}).should("be.visible").click()
    }

    clickUnarchiveBtn() {
        cy.get('#checked-toolbar>#topics-unarchive-btn', {timeout: 50000}).should("be.visible").click()
    }

    verify_the_base_after_unarchive(base) {
        cy.xpath("//a[@class='cell topic-name '][contains(text(),'" + base["base_name"] + "')]", {timeout: 50000})
            .should('not.be.visible')
    }

    selectActiveOption() {
        cy.xpath("//div[text()='Active']", {timeout: 50000}).should("be.visible").click()
    }

    verifyArchivedQuestionnaire(base, questionnaire, randomNumber) {
        cy.xpath("//div[@data-topic='" + base + "']/../a[normalize-space()=' " + questionnaire['questionnaire_name'] + " " + randomNumber + "']")
            .should('not.exist')
    }

    verify_Created_base_on_topic_filter_field(base) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + base + "']")
            .should('not.exist')
    }

    clickBaseExpandIcon(baseName) {
        cy.xpath("//div[@class='flex-row']//a[contains(text(),'" + baseName + "')]/../div/ul/li[@class='detail-toggle-btn']/a[@title='Detail']", {timeout: 15000})
            .should('be.visible')
            .should("be.visible").click()
    }

    verifyTheAuthorName(admin) {
        cy.xpath("//div[@class='data-value' and text()='" + admin["first_name"][0].toUpperCase() + admin["first_name"].slice(1) + " " + admin['last_name'][0].toUpperCase() + admin["last_name"].slice(1) + "']")
            .should('be.exist')
    }

    verifyTheEditedAuthorName(lastName, firstName) {
        let firstname = firstName[0].toUpperCase()
        let lastname = lastName[0].toUpperCase()
        cy.xpath("//div[@class='data-value' and text()='" + firstname + "" + firstName.slice(1) + " " + lastname + "" + lastName.slice(1) + "']")
            .should('be.visible')
    }

    verifytheCreatedDomainName(domain) {
        cy.wrap(domain).each((data) => {
            cy.xpath("//div[@class='data-group  ']//div[text()='" + data + "']", {timeout: 50000})
                .should('be.visible')
        })

    }

    verifyTHeDomainName(baseName, domains) {
        cy.xpath("//div[@class='flex-row'][a[contains(text(),'" + baseName + "')]]/../div//label[text()='Domains']/following::div[1]").then((data) => {
            const datas = data.text();
            const textspace = datas.replace(/\s*,\s*/g, ",");
            const sortedItems = domains.slice().sort();
            const items = sortedItems.toString();
            expect(textspace).to.equal(items)
        })

    }

    clickCollapseIcon(baseName) {
        cy.xpath("//div[@class='flex-row']//a[contains(text(),'" + baseName + "')]/../div/ul/li/a[@data-toggle='collapse']", {timeout: 15000})
            .should('be.visible')
            .click()
    }

    clickEditBaseIcon() {
        cy.xpath("//div[@id='std-toolbar']//a[text()='Edit']", {timeout: 50000}).should("be.visible").click()
    }

    clickAuthorField() {
        cy.get('#author').should("be.visible").click();
    }

    chooseUser(lastName, firstName) {
        let firstname = firstName[0].toUpperCase()
        let lastname = lastName.toUpperCase()
        cy.xpath("//div[@class='user-item']//span[text()='" + lastname + " " + firstname + "" + firstName.slice(1) + "']")
            .should("be.visible").click()
    }

    verifyTheSelectedUser(user) {
        let firstname = user[0]["first_name"][0].toUpperCase()
        let lastname = user[0]["last_name"].toUpperCase()
        cy.xpath("//input[@id='author' and @value='" + lastname + " " + firstname + "" + user[0]['first_name'].slice(1) + "']")
            .should('be.exist')
    }

    edit_base_name(base) {
        cy.get('#topic_text',{timeout: 50000}).should('be.visible').clear().type(base);
    }


    verifyCreatedBaseQuestions(question) {
        cy.wrap(question).each((question_title) => {
            cy.xpath("//div[@class='cell title'][contains(normalize-space(.),'" + question_title + "')]", {timeout: 50000})
                .should('not.exist')
        })
    }

    clickEnterPriseFolderDD() {
        cy.get('#topic-filter-button',{timeout: 50000}).should('be.visible').click()
    }

    verifyCreatedBaseMedias(media, randomNumber) {
        cy.wrap(media).each((media_name) => {
            cy.xpath("//div[@id='media-folder']//div[contains(@class,'folder-item media tags-filtered')]//div//span[text()=' " + media_name + " " + randomNumber + "']", {timeout: 50000})
                .should('not.exist')
        })
    }

    click_topic_filter_field_on_Training_page() {
        cy.xpath("//span[@id='topic-filter-button']",{timeout: 50000}).should('be.visible')
            .click()
    }

    verifyCreatedBaseTraining(training) {
        cy.xpath("//div//a[normalize-space()=' " + training['training_name'] + " ']", {timeout: 50000})
            .should('not.exist');
    }

    clickAddIcon() {
        cy.get('#domains > div.bloc-body > div:nth-child(2) > div.cell.add-domain',{timeout: 50000}).should('be.visible').click()
    }

    enterAbbreviation(abbreviation) {
        cy.get('#topic_short',{timeout: 50000}).should('be.visible').type(abbreviation)
    }

    enterDescription(description) {
        cy.get('#description',{timeout: 50000}).should('be.visible').type(description)
    }

    //To delete Base
    clickDeleteIcon(base) {
        cy.xpath("//div//a[normalize-space()='" + base + "']/../../div//div[6]//ul//li[5]",{timeout: 50000}).should('be.visible').click();
    }

    clickDeleteButton() {
        cy.get('a.btn.btn-danger.btn-ok',{timeout: 50000}).should('be.visible').click();
    }

    clickModuleTabOnBase() {
        cy.xpath("//li//a[text()='Modules']",{timeout: 50000}).should('be.visible').click();
    }

    clickNewModuleButton() {
        cy.get('#new-module-btn',{timeout: 50000}).should('be.visible').click();
    }

    verifyDeletedModuleName(base, randomNo) {
        cy.xpath("//div[@class='flex-row body']//a[@class='cell module-name ellipsis '][contains(normalize-space(),'" + base + "')]")
            .should('not.be.exist');
    }


    clickTrainingModule() {
        cy.get('#topic-view-page>div>ul>li:nth-child(5)>a',{timeout: 50000}).should('be.visible').click()
    }

    clickNewTrainingBtn() {
        cy.get('#new-training-btn',{timeout: 50000}).should('be.visible').click()
    }

    enterTrainingName(base) {
        cy.get('#training_name',{timeout: 50000}).should('be.visible').type(base['training_name'])
    }

    chooseStartDate() {
        const dayjs = require('dayjs')
        cy.get('#start_date').type(dayjs().format('DD/MM/YYYY'))
    }

    chooseEndDate() {
        const dayjs = require('dayjs')
        cy.get('#end_date').type(dayjs().format('DD/MM/YYYY'))
    }

    enterTrainerName() {
        cy.get('#trainer_name',{timeout: 50000}).should('be.visible').click()
        cy.get('div.users-div',{timeout: 50000}).should('be.visible').click()
        cy.get('#trainer_name', {timeout: 50000}).should('be.visible')
    }

    clickSave() {
        cy.get('#standard-toolbar>button',{timeout: 50000}).should('be.visible').click()
    }

    chooseQuestionnaire() {
        cy.xpath("//div[@class='big-btn  with-href ']//span[text()='questionnaire']",{timeout: 50000}).should('be.visible').click()
    }

    dragQuestionaire() {
        cy.get('#items-set-1>div',{timeout: 50000}).should('be.visible').drag('#items-set-2')
    }

    clickMediaTab() {
        cy.xpath("//li//a[text()='Medias']",{timeout: 50000}).should('be.visible').click()
    }
}

export default BasePage;
