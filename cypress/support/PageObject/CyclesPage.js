import moment from "moment";

class CyclesPage {
    //To click the cycles menu from the side menu
    click_cycles_side_menu() {
        cy.get("#nav-main ul li a span", { timeout: 10000 }).contains("Cycles").click()
    }

    //To click the new cycle button
    click_new_cycle_button() {
        cy.xpath("//div//a[@id='new-cycle-btn']", { timeout: 10000 }).should("be.visible").click()
    }

    //To enter the cycle name
    enter_cycle_name(cycledata) {
        cy.xpath("//div[@class='bloc-body']//div//div//label[contains(text(),'Name')]//..//input[@id='cycle_name']", { timeout: 10000 })
            .should("be.visible").type(cycledata["cycle_name"])
    }

    //To enter the cycle description
    enter_description(cycledata) {
        let cycle_description = cycledata.cycle_1;
        cy.xpath("//div[@class='bloc-body']//div[@class='flex-row']//label[contains(text(),'Description')]//..//textarea[@id='description']", { timeout: 10000 }).should("be.visible")
            .type(cycle_description["description"])
    }

    //To click the save button at the cycle creation page
    click_cycle_save_button() {
        cy.xpath("//button[@type='submit'][@form='cycle-form']", { timeout: 10000 }).should("be.visible").click()
    }

    //To verify created cycle is displayed at cycle list page
    verify_created_cycle(cycledata) {
        cy.xpath("//div[@class='flex-row body']//a[@class='cell cycle-name ellipsis'][contains(text(),'" + cycledata["cycle_name"] + "')]", { timeout: 10000 })
            .should('be.visible')
    }

    //To open created cycles
    open_created_cycles(cycledata) {
        cy.xpath("//div[@class='flex-row body']//a[@class='cell cycle-name ellipsis'][contains(text(),'" + cycledata["cycle_name"] + "')]", { timeout: 10000 })
            .should("be.visible").click()
    }

    //To click steps tab at cycles
    click_steps_tab() {
        cy.xpath("//div[@class='tabs-wrapper']//ul//li//a[text()='Steps']", { timeout: 10000 }).should("be.visible")
            .click()
    }

    //To filter the created base at steps page of cycles
    filter_created_base_on_steps_page(basedatas) {
        this.click_topic_filter_field()
        this.select_all_topic_option()
        this.click_topic_filter_field()
        this.select_base_on_topic_filter_field(basedatas)
    }

    filter_all_on_steps_page() {
        this.click_topic_filter_field()
        this.select_all_topic_option()
    }

    //To click the topic filter
    click_topic_filter_field() {
        cy.xpath("//*[@id='topic-filter-button']", { timeout: 10000 }).should("be.visible").click();

    }

    //To select the base on the topic filter
    select_base_on_topic_filter_field(basedatas) {
        cy.xpath("//ul[@id='topic-filter-menu']//div[text()='" + basedatas["base_name"] + "']", { timeout: 10000 }).should("be.visible").click();

    }

    //To select all options
    select_all_topic_option() {
        cy.xpath("//div[contains(@id,'ui-id')][contains(text(),'All topics')]", { timeout: 10000 }).should("be.visible").click();
    }


    //To add the questionnaire to the cycles
    select_questionnaire_to_cycle(steps) {
        cy.xpath("//span[@class='caption'][contains(text(),'" + steps["questionnaire_name"] + "')]//..//..//..//a[@class='item-select-btn fa fa-arrow-circle-o-right']", { timeout: 10000 })
            .should("be.visible").click()
    }


    select_form_on_cycle(form) {
        cy.xpath("//div[@id='available-items']//span[contains(text(),'" + form["form_name"] + "')]/../../..//a[contains(@title,'Add this survey to the cycle')]", { timeout: 10000 })
            .should("be.visible").click()
    }

    //To click the save button at the step page of the cycles
    click_save_button_cycles_step_page() {
        cy.xpath("//a[@id='save-btn'][contains(text(),'Save')]", { timeout: 10000 }).should("be.visible")
            .click()
        this.reload();
    }

    //To refersh page
    reload() {
        cy.reload();
    }

    set_delay_as_persent_on_questionnaire(base, questionnaire) {
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div/input[@name='delta_days']", { timeout: 10000 })
            .should("be.visible").clear()
            .type('0')
    }

    set_delay_as_persent_on_form(form) {
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + form["form_name"] + "')]/../../..//div/input[@name='delta_days']", { timeout: 10000 })
            .should("be.visible").clear()
            .type('0')
    }


    uncheck_all_days(base, questionnaire) {
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Monday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Monday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Tuesday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Tuesday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Wednesday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Wednesday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Thursday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Thursday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").click()
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Friday'][@class='checked']", { timeout: 10000 })
            .should('be.visible')
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//div//ul/li[@title='Friday'][@class='checked']", { timeout: 10000 })
            .should("be.visible").click()
    }

    set_day_as_persent(base, questionnaire) {
        const weekday = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
        const today_date = new Date()
        const day = weekday[today_date.getDay()]
        cy.log(day)
        cy.xpath("//div[@id='selected-items']//span[contains(.,'" + questionnaire["questionnaire_name"] + "')]/../span[contains(.,'" + base["base_name"] + "')]/../../..//ul/li[text()='" + day + "']", { timeout: 10000 })
            .should("be.visible").click()
    }

    click_invitation_tab_on_cycle_invitation_page() {
        cy.xpath("//a[text()='Invitations']", { timeout: 10000 }).should("be.visible").click()
    }

    enable_yes_on_cycle_invitation_page() {
        cy.xpath("//span[text()='Yes']", { timeout: 10000 }).should("be.visible").click()
    }

    enter_subject_on_cycle_invitation_page(mail_sub, rand) {
        cy.xpath("//input[@id='email_subject']", { timeout: 10000 }).should("be.visible")
            .clear()
            .type(mail_sub["mail_subject"] + rand["rand_no"])
    }

    enter_message_on_cycle_invitation_page(mail_sub) {
        cy.xpath("//textarea[@id='email_message']", { timeout: 10000 }).should("be.visible")
            .clear()
            .type(mail_sub["mail_message"])
    }

    click_emails_and_mobile_button() {
        cy.get('#select-contacts-btn', { timeout: 10000 }).should("be.visible").click();
    }

    enter_contact_user_mail_id(emails) {
        cy.get('#contacts-participants', { timeout: 10000 }).should("be.visible")
            .type(emails["contact_user_id"] + emails["domain"], { waitForAnimations: false })
    }

    click_save_button_on_emails_and_mobile_window() {
        cy.get('#save-contacts', { timeout: 10000 }).should("be.visible").click();

    }

    click_invite_button_on_cycle_invitation_page() {
        cy.xpath("//button[@id='invite']", { timeout: 10000 }).should("be.visible").click();
    }

    click_users_tab_on_cycle() {
        cy.xpath("//a[text()='Users']", { timeout: 10000 }).should("be.visible").click()
    }

    click_status_filter_field() {
        cy.xpath("//span[@id='status-filter-button']", { timeout: 10000 }).should("be.visible").click()
    }

    filter_ongoing_status_on_cycle_user_page() {
        this.click_status_filter_field()
        cy.xpath("//ul[@id='status-filter-menu']//div[contains(text(),'Ongoing')]", { timeout: 10000 }).should("be.visible").click()
    }

    filter_all_status_on_cycle_user_page() {
        this.click_status_filter_field()
        cy.xpath("//ul[@id='status-filter-menu']//div[contains(text(),'All')]", { timeout: 10000 }).should("be.visible").click()
    }

    filter_canceled_status_on_cycle_user_page() {
        cy.wait(5000)
        this.click_status_filter_field()
        cy.xpath("//ul[@id='status-filter-menu']//div[contains(text(),'Cancelled')]", { timeout: 10000 }).should("be.visible").click()
    }

    verify_contact_user_played_cycle_test(contact) {
        cy.xpath("//div[text()='[" + contact["contact_user_id"] + "]']/..//div[text()='completed']", { timeout: 10000 })
            .should('be.exist')
    }

    verify_active_user_played_cycle_result(user) {
        cy.wrap(user).each(userdata => {
            cy.xpath("//div[text()='" + userdata["last_name"] + " " + userdata["first_name"] + "']/..//div[text()='completed']", { timeout: 10000 })
                .should('be.exist')
        })
    }

    click_select_user_button_on_cycle_invitation_page() {
        cy.xpath("//*[@id='select-employees-btn']", { timeout: 10000 }).should("be.visible").click()
    }

    select_active_user(userdata) {
        cy.wrap(userdata).each(user => {
            cy.xpath("//span[@class='username'][contains(text(),'" + user["last_name"].toUpperCase() + " " + user["first_name"][0].toUpperCase() + user["first_name"].slice(1) + "')]/../../div/a[@class='to-right-btn']", { timeout: 10000 })
                .should("be.visible").click()
        })
    }

    click_validate_button_on_select_user_popup() {
        cy.xpath("//*[@id='select-multi-users-modal']//a[text()=' Validate']", { timeout: 10000 }).should("be.visible").click()
    }


    click_checkbox_on_ongoing_cycle() {
        cy.xpath("//div[@class='cell check']//span[@class='fa fa-square-o unchecked  ']", { timeout: 10000 }).should("be.visible").click()
    }

    click_delete_button_on_cycle_user_page() {
        cy.xpath("//a[@title='Delete all checked']", { timeout: 10000 }).should("be.visible").click()
    }

    click_delete_button_on_confirmation_cycle_user_page() {
        cy.xpath("//a[@id='confirm-delete-btn']", { timeout: 10000 }).should("be.visible").click()
    }

    verify_deleted_ongoing_cycle_is_exist_on_canceled_status(user) {
        cy.wrap(user).each(userdata => {
            cy.xpath("//div[text()='" + userdata["first_name"][0].toUpperCase() + userdata["first_name"].slice(1) + " " + userdata["last_name"][0].toUpperCase() + userdata["last_name"].slice(1) + "']//following::div[text()='cancelled']", { timeout: 10000 })
                .should('be.exist')
        })
    }
}


export default CyclesPage