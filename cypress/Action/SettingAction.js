import SettingPage from '../support/PageObject/SettingPage.js';
import UserPage from "../support/PageObject/UserPage.js";

const settingpage = new SettingPage()
const userpage = new UserPage()


class SettingAction {

    enable_only_administration_side_menu_and_verify(){
        settingpage.enable_only_administration_side_menu()
        settingpage.verify_all_administration_side_menu()
    }

    enable_all_settings_option_and_verify_side_menu(admindata, settingdata, common_data){
        settingpage.enable_only_administration_side_menu()
        settingpage.enable_only_administrator_dashboard()
        settingpage.enable_only_enterprise(admindata, settingdata, common_data,)
        settingpage.enable_only_specific_function()
        settingpage.enable_only_player()
        settingpage.enable_only_bases()
        settingpage.enable_only_medias()
        settingpage.enable_only_questionnaires()
        settingpage.enable_only_skills()
        settingpage.enable_only_trainings()
        settingpage.enable_only_exports(settingdata)
        settingpage.enable_only_user_data()
        settingpage.verify_all_administration_side_menu()
    }

    enable_all_user_type_option_and_their_side_menu(){
        settingpage.click_setting_side_menu()
        settingpage.enable_all_user_type()
        settingpage.enable_all_user_type_side_menu()
    }

    get_password_from_preview_message_on_email_and_log_tab(usersdatas, mail_sub) {
        settingpage.click_setting_side_menu()
        settingpage.click_administration_tab()
        settingpage.click_emails_and_logs_tab_on_administration_page()
        cy.wrap(usersdatas).each((user) => {
            settingpage.click_preview_message_button_on_emails_and_logs_tab(user, mail_sub)
        })
        // settingpage.get_password_from_activation_mail_sub()
        settingpage.get_password_from_email_Sms_log()
        settingpage.log_out_admin()
    }

    get_user_registration_link_on_email_and_log_tab(usersdatas, mail_sub, randno) {
        settingpage.click_setting_side_menu()
        settingpage.click_administration_tab()
        settingpage.click_emails_and_logs_tab_on_administration_page()
        cy.wrap(usersdatas).each((user) => {
            settingpage.click_preview_message_button_for_registration_link_on_emails_and_logs_tab(user, mail_sub, randno)
        })
        settingpage.get_registration_link_from_invited_user_mail_sub()
        settingpage.log_out_admin()

    }

    get_contact_user_of_survey_play_link_on_email_and_log_tab(contactuser, mail_sub, randno) {
        settingpage.click_setting_side_menu()
        settingpage.click_administration_tab()
        settingpage.click_emails_and_logs_tab_on_administration_page()
        settingpage.click_preview_message_button_for_survey_test_for_contactuser_on_emails_and_logs_tab(contactuser, mail_sub, randno)
        settingpage.get_registration_link_from_invited_user_mail_sub()
        settingpage.log_out_admin()

    }

    get_contact_user_of_questionnaire_play_link_on_email_and_log_tab(contactuser, mail_sub, randno) {
        settingpage.click_setting_side_menu()
        settingpage.click_administration_tab()
        settingpage.click_emails_and_logs_tab_on_administration_page()
        settingpage.click_preview_message_button_for_questionnaire_test_for_contactuser_on_emails_and_logs_tab(contactuser, mail_sub, randno)
        settingpage.get_registration_link_from_invited_user_mail_sub()
        settingpage.log_out_admin()
    }

    get_contact_user_of_cycle_event_play_link_on_email_and_log_tab(contactuser, mail_sub, randno) {
        settingpage.click_setting_side_menu()
        settingpage.click_administration_tab()
        settingpage.click_emails_and_logs_tab_on_administration_page()
        settingpage.click_preview_message_button_for_cycle_events_of_questionnaire_test_for_contactuser_on_emails_and_logs_tab(contactuser, mail_sub)
        settingpage.get_cycle_event_of_questionnaire_link_from_invited_user_mail_sub()
        // settingpage.reload_email_and_sms_page_until_cycle_event_log(contactuser)
        settingpage.click_preview_message_button_for_cycle_events_of_survey_test_for_contactuser_on_emails_and_logs_tab(contactuser, mail_sub)
        settingpage.get_cycle_event_of_survey_link_from_invited_user_mail_sub()
        settingpage.click_preview_message_button_for_cycle_for_contactuser_on_emails_and_logs_tab(contactuser, mail_sub, randno)
        settingpage.verify_cycle_message_on_emails_and_logs_tab(mail_sub, randno)
        settingpage.log_out_admin()
    }


}
export  default SettingAction;



