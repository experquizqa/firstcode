import TrainingsPage from "../support/PageObject/TrainingsPage";
import CyclesPage from "../support/PageObject/CyclesPage";
import UserPage from "../support/PageObject/UserPage";
import SurveysPage from "../support/PageObject/SurveysPage";
import QuestionnairePage from "../support/PageObject/QuestionnairePage";


const trainingspage = new TrainingsPage();
const cyclespage = new CyclesPage();
const userpage = new UserPage()
const surveypage = new SurveysPage();
const questionnairepage = new QuestionnairePage();



class TrainingAction
{
    //To create trainings
    create_trainings_with_cycle(basedatas,trainingdata,admindatas,cycledata){
        trainingspage.click_trainings_side_menu();
        cyclespage.filter_created_base_on_steps_page(basedatas);
        trainingspage.click_new_training_button();
        trainingspage.enter_training_name(trainingdata);
        trainingspage.enter_start_date_as_current_date_in_trainings();
        trainingspage.enter_end_date_as_tomorrow_date_in_trainings();
        trainingspage.click_trainer_field();
        trainingspage.choose_trainer(admindatas);
        trainingspage.choose_cycles_for_participants(cycledata);
        trainingspage.choose_cycles_for_trainer(cycledata);
        trainingspage.choose_cycles_for_managers(cycledata);
        trainingspage.click_start_of_cycle_mode_on_training_create_page()
        trainingspage.select_when_inviting_option_on_start_of_cycle()
        trainingspage.click_save_button_at_training_creation_page(admindatas);
        trainingspage.verify_created_training(trainingdata,basedatas);
    }

    //To create trainings without cycle
    create_trainings_without_cycle(basedatas, training, admindatas){
        trainingspage.click_trainings_side_menu();
        cyclespage.filter_created_base_on_steps_page(basedatas);
        trainingspage.click_new_training_button();
        trainingspage.enter_training_name(training);
        trainingspage.enter_start_date_as_current_date_in_trainings();
        trainingspage.enter_end_date_as_tomorrow_date_in_trainings();
        trainingspage.click_trainer_field();
        trainingspage.choose_trainer(admindatas);
        trainingspage.click_save_button_at_training_creation_page(admindatas);
        trainingspage.verify_created_training(training,basedatas);
    }

    drag_questionnaire_on_practice_section_in_training(basedatas,trainingdata, training_ques){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_questionnaire_tab_on_opened_training()
        trainingspage.drag_questionnaire_to_practice_section_on_training(training_ques)
    }

    drag_cycle_questionnaire_on_practice_section_in_training(basedatas,trainingdata, cy_ques){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_questionnaire_tab_on_opened_training()
        trainingspage.drag_cycle_questionnaire_to_practice_section_on_training(cy_ques)
    }

    //To add participants into training
    add_participants_into_created_training(basedatas,usersdatas,trainingdata){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_invitation_tab();
        trainingspage.click_select_participants_button();
        cy.wrap(usersdatas).each((data) => {
            trainingspage.choose_participants(data.first_name,data.last_name);
        })
        trainingspage.click_validate_button();
    }

    invite_selected_participant_on_training(basedatas,usersdatas,trainingdata){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_invitation_tab()
        cy.wrap(usersdatas).each((data) => {
            trainingspage.selected_participant_is_on_not_send_status(data.first_name,data.last_name);
            trainingspage.click_send_invitation_button()
            trainingspage.verify_user_is_on_send_status_after_inviting_on_training(data.first_name,data.last_name)
        })
        userpage.log_out_user()

    }

    verify_practice_questionnaire_test_result_on_training_result_page(basedatas, trainingdata, userdata){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_result_tab_on_training()
        trainingspage.click_report_tab_on_training_result_page()
        trainingspage.verify_practice_training_result(userdata)
    }

    verify_cycle_event_of_questionnaire_result_on_training_of_cycle_tab(basedatas, trainingdata, userdata, admin){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_result_tab_on_training()
        trainingspage.click_cycle_tab_on_training_result_page()
        trainingspage.verify_cycle_result_on_training_result_page(userdata, admin)
    }

    verify_evaluation_questionnaire_test_result_on_training_result_page(basedatas, trainingdata, userdata, training_ques){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_result_tab_on_training()
        trainingspage.click_report_tab_on_training_result_page()
        trainingspage.verify_evaluation_training_result_on_report_tab(userdata)
        trainingspage.click_evaluation_tab_on_training_result_page()
        trainingspage.verify_evaluation_training_result_on_evaluation_tab(training_ques)
    }

    verify_cycle_event_of_survey_result_on_training_page(basedatas, trainingdata, survey){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_survey_tab_on_training()
        trainingspage.verify_survey_result_on_training_page(survey)
    }

    invite_training_as_evaluation_to_active_user(basedatas, trainingdata, training_ques){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_questionnaire_tab_on_opened_training()
        trainingspage.drag_questionnaire_to_used_section_on_training(training_ques)
        trainingspage.click_see_as_trainer_button()
        trainingspage.click_questionnaire_tab_on_opened_training()
        trainingspage.invite_questionnaire_as_evaluation_on_training()
        userpage.log_out_user()
    }

    DragandDrop_present_user_to_reg_user_section_on_training_participant_page(basedatas, trainingdata, user){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_participants_tab()
        trainingspage.drag_and_drop_user_on_present_section(user)
    }

    invite_cycle_for_selected_participant_on_training(basedatas, trainingdata, user){
        trainingspage.click_trainings_side_menu();
        trainingspage.open_created_training(basedatas, trainingdata);
        trainingspage.click_participants_tab()
        trainingspage.drag_and_drop_user_on_present_section(user)
        trainingspage.click_invitation_tab()
        trainingspage.click_cycle_invitation_button_on_training()
        userpage.log_out_user()
    }

    play_cycle_event_by_admin(cycle, question, survey){
        trainingspage.click_user_view_menu_on_admin()
        let cyc = cycle.cycle_step
        cy.wrap(cyc).each((stepdata)=> {
            cy.log('my type is-----', stepdata.type)
            if(stepdata.type === "questionnaire") {
                userpage.click_questionnaire_invitation(stepdata)
                questionnairepage.verify_start_button_before_playing_static_questionnaire()
                questionnairepage.select_answers_in_static_player_by_active_user(question)
            }
            else if(stepdata.type === "form") {
                userpage.click_survey_invitation(stepdata)
                surveypage.fill_form(survey)
            }
        })
        userpage.log_out_user()
    }



}
export default TrainingAction