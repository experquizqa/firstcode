import PathPage from "../support/PageObject/PathPage";
import SurveysPage from "../support/PageObject/SurveysPage";
import HomePage from "../support/PageObject/HomePage.js";
import UserPage from "../support/PageObject/UserPage.js";

const survyespage = new SurveysPage();
const homepage = new HomePage();
const userpage = new UserPage();

class SurveysAction{

    //To create surveys
    create_surveys(surveydata){
        survyespage.click_surveys_side_menu();
        survyespage.click_new_form_button();
        survyespage.enter_survey_name(surveydata);
        survyespage.click_save_button_at_survey_creation_page();
        // survyespage.verify_success_message_after_survey_saved(surveydata);
    }

    create_all_fields(surveydata){
        survyespage.click_surveys_side_menu();
        survyespage.open_created_survey(surveydata);
        survyespage.click_edit_button();
        survyespage.create_all_fields_on_created_surveys(surveydata)
        survyespage.click_save_button_at_survey_creation_page();
    }

    create_fields_with_condition(conditiondata){
        survyespage.click_surveys_side_menu();
        survyespage.open_created_survey(conditiondata);
        survyespage.click_edit_button();
        survyespage.create_fields_with_condition(conditiondata)
        survyespage.click_save_button_at_survey_creation_page();
        survyespage.click_surveys_side_menu();
    }

    trial_play_on_created_survey(surveydata){
        survyespage.click_surveys_side_menu();
        survyespage.click_trial_play_on_survey_list_page(surveydata)
        survyespage.fill_form(surveydata)
    }

    trial_play_on_condition_survey(conditiondata){
        survyespage.click_surveys_side_menu();
        survyespage.click_trial_play_on_survey_list_page(conditiondata)
        survyespage.fill_form_with_condition(conditiondata)
    }

    invite_survey_to_activated_user(surveydata, userdata){
        survyespage.click_surveys_side_menu();
        survyespage.open_created_survey(surveydata);
        survyespage.click_invitation_tab_on_survey()
        survyespage.click_select_user_button_on_survey_invitation_page()
        survyespage.select_activated_user_on_survey_invitation_page(userdata)
        survyespage.click_validate_buttton_on_survey_invitation_page()
        survyespage.click_invite_buttton_on_survey_invitation_page()
        homepage.log_out_admin()

    }

    invite_survey_to_contact_user(surveydata, contactuser, randno, mail_sub){
        survyespage.click_surveys_side_menu();
        survyespage.open_created_survey(surveydata);
        survyespage.click_invitation_tab_on_survey()
        userpage.enter_mail_subject(mail_sub, randno)
        userpage.enter_mail_message(mail_sub)
        survyespage.click_email_and_mobile_button_on_survey_invitation_page()
        survyespage.enter_user_mailid_on_email_and_mobile_popup(contactuser)
        survyespage.click_save_button_on_email_and_mobile_popup()
        survyespage.click_invite_buttton_on_survey_invitation_page()
        homepage.log_out_admin()
    }

    play_survey_by_user(surveydata){
        survyespage.fill_form(surveydata)
        userpage.log_out_user()

    }

    play_survey_by_contact_users(surveydata){
        survyespage.fill_form(surveydata)

    }

    play_survey_by_contact_user(surveydata, link){
        survyespage.fill_form(surveydata)
        survyespage.verify_contact_user_is_on_the_return_link_page_after_completed_form(link)

    }

    verify_survey_result_after_user_completed_form_test(surveydata, userdata){
        survyespage.click_surveys_side_menu();
        survyespage.open_created_survey(surveydata);
        survyespage.click_result_tab_on_survey()
        survyespage.verify_status_on_played_survey_on_survey_result_page(userdata)
        survyespage.click_detail_button_on_survey_result_page(userdata)
        survyespage.verify_result_data_on_survey(surveydata, userdata)

    }

    verify_survey_result_after_contactuser_completed_form_test(surveydata, contactuser){
        survyespage.click_surveys_side_menu();
        survyespage.open_created_survey(surveydata);
        survyespage.click_result_tab_on_survey()
        survyespage.verify_status_on_played_contact_user_on_survey_result_page(contactuser)
        survyespage.click_detail_button_on_contact_user_of_survey_result_page(contactuser)
        survyespage.verify_result_data_for_contactuser_on_survey(surveydata, contactuser)

    }




}
export default SurveysAction