import BasePage from "../support/PageObject/BasePage.js";
import TabsPageSpec from "../support/PageObject/TabsPage.spec";
import AdminDashboardPage from "../support/PageObject/AdminDashboardPage.js";
import QuestionnairePage from "../support/PageObject/QuestionnairePage.js";
import MediaPage from "../support/PageObject/MediaPage.js";
import CyclesPage from "../support/PageObject/CyclesPage";
import TrainingsPage from "../support/PageObject/TrainingsPage";
import ModulesPage from "../support/PageObject/ModulesPage";
import PathPage from "../support/PageObject/PathPage";
import SurveysPage from "../support/PageObject/SurveysPage";
import CertificatesPage from "../support/PageObject/CertificatesPage";
import SettingPage from "../support/PageObject/SettingPage.js";
import HomePage from "../support/PageObject/HomePage.js";


const basepage = new BasePage();
const TabsPage = new TabsPageSpec();
const admindashboardpage = new AdminDashboardPage();
const questionnairepage = new QuestionnairePage();
const mediapage = new MediaPage();
const cyclespage = new CyclesPage();
const trainingspage = new TrainingsPage();
const modulespage = new ModulesPage() ;
const pathspage = new PathPage();
const survyespage = new SurveysPage();
const certificatespage = new CertificatesPage();
const settingpage = new SettingPage()
const homepage = new HomePage();

class HomeTabsAction
{
    //To verify all the tabs at the home page
    verify_all_tabs_at_home_page()
    {
        TabsPage.home_button();
        TabsPage.solution_button();
        TabsPage.testimonials_button();
        TabsPage.pricing_button();
        TabsPage.contact_button();
        TabsPage.free_trail_button();
        TabsPage.connection_button();
        TabsPage.language_button();
        // Home.free_trail_button_at_bottom();
    }

    //To verify all tabs at the dashboard page
    verify_all_tabs_at_dashboard_page()
    {
        TabsPage.click_dashboard_menu();
        TabsPage.base_count_tab();
        TabsPage.questionnaire_count_tab();
        TabsPage.question_count_tab();
        TabsPage.user_count_tab();
        TabsPage.special_user_count_tab();
        TabsPage.group_count_tab();
    }

    //To verify all tabs at the base page
    verify_all_tabs_at_base_page(base)
    {
        basepage.open_created_base(base);
        TabsPage.base_overview_tab();
        TabsPage.base_questionnaires_tab();
        TabsPage.base_questions_tab();
        TabsPage.base_results_tab();
        TabsPage.base_trainings_tab();
        TabsPage.base_modules_tab();
        TabsPage.base_medias_tab();
        TabsPage.base_import_export_tab();
        TabsPage.base_quality_tab();
        TabsPage.click_base_result_tab();
        TabsPage.base_per_evaluation_tab();
        TabsPage.base_per_user_tab();
        TabsPage.base_free_and_public_tests_tab();
    }

    //To verify all tabs at the questionnaire page
    verify_all_tabs_at_questionnaire_page(basedatas,questionnaire)
    {
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnaire)
        TabsPage.questionnaire_overview_tab();
        TabsPage.questionnaire_questions_tab();
        TabsPage.questionnaire_selection_tab();
        TabsPage.questionnaire_options_tab();
        TabsPage.questionnaire_invitation_tab();
        TabsPage.questionnaire_visibility_tab();
        TabsPage.questionnaire_results_tab();
        admindashboardpage.click_admin_questionnaire_menu()
        questionnairepage.filter_created_base_on_questionnaire_list_page(basedatas)
        questionnairepage.open_created_questionnaire(questionnaire)
        TabsPage.verify_tab_on_questionnaire_results_page()

    }

    //To verify all tabs at media page
    verify_all_tabs_at_media_page()
    {
        mediapage.click_media_side_menu()
        TabsPage.medias_tab();
        TabsPage.visibility_tab();
    }

    //To verify all tabs at users page
    verify_all_tabs_at_users_page(admindatas)
    {
        basepage.click_admin_user_menu();
        TabsPage.users_users_tab();
        TabsPage.users_groups_tab();
        TabsPage.users_invite_tab();
        TabsPage.users_mailing_tab();
        TabsPage.users_contacts_tab();
        TabsPage.users_imports_exports_tab();
        TabsPage.users_hierarchy_tab();
        TabsPage.users_renewals_tab();
        TabsPage.click_see_activities_button(admindatas);
        TabsPage.see_activities_overview_tab();
        TabsPage.see_activities_results_tab();
        TabsPage.see_activities_trainings_tab();
        TabsPage.see_activities_modules_tab();
        TabsPage.see_activities_paths_tab();
        TabsPage.see_activities_skills_tab();
        TabsPage.see_activities_users_audit_log_tab();

    }

    //To verify all the tabs at the results page
    verify_all_tabs_at_result_page()
    {
        TabsPage.click_results_menu();
        TabsPage.results_per_evaluation_tab();
        TabsPage.results_per_user_tab();
        TabsPage.results_free_and_public_tests_tab();
    }

    //To verify all the tabs at the cycles page
    verify_all_tabs_at_cycles_page(cycledata){
        cyclespage.click_cycles_side_menu();
        cyclespage.open_created_cycles(cycledata);
        TabsPage.cycles_overview_tab();
        TabsPage.cycles_steps_tab();
        TabsPage.cycles_invitations_tab();
        TabsPage.cycles_users_tab();
    }

    //To verify all the tabs at skills page
    verify_all_tabs_at_skills_page(){
        TabsPage.click_skills_side_menu();
        TabsPage.skills_skills_tab();
        TabsPage.skills_qualifications_tab();
        TabsPage.skills_jobs_tab();
        TabsPage.skills_overview_tab();
        TabsPage.skills_users_tab();
        TabsPage.skills_import_tab();
    }

    //Verify all tabs a training before add participants
    verify_trainings_tab_before_add_participants(trainingdata,basedatas){
        trainingspage.click_trainings_side_menu();
        cyclespage.filter_created_base_on_steps_page(basedatas);
        trainingspage.open_created_training(trainingdata,basedatas);
        TabsPage.training_overview_tab();
        TabsPage.training_questionnaires_tab();
        TabsPage.training_medias_tab();
        TabsPage.training_invitations_tab();
        TabsPage.training_participants_tab();
        TabsPage.training_surveys_tab();
        TabsPage.click_see_as_trainer_button();
        TabsPage.training_overview_tab();
        TabsPage.training_questionnaires_tab();
        TabsPage.training_medias_tab();
        TabsPage.training_participants_tab();
        TabsPage.training_surveys_tab();
        TabsPage.click_see_as_admin_button();
    }

    //Verify all tabs a training after added participants
    verify_trainings_tab_after_added_participants(trainingdata,basedatas){
        trainingspage.click_trainings_side_menu();
        cyclespage.filter_created_base_on_steps_page(basedatas);
        trainingspage.open_created_training(trainingdata,basedatas);
        TabsPage.training_overview_tab();
        TabsPage.training_questionnaires_tab();
        TabsPage.training_medias_tab();
        TabsPage.training_invitations_tab();
        TabsPage.training_participants_tab();
        TabsPage.training_chat_tab();
        TabsPage.training_black_board_tab();
        TabsPage.training_results_tab();
        TabsPage.training_attendance_sheet_tab();
        TabsPage.training_surveys_tab();
        TabsPage.click_see_as_trainer_button();
        TabsPage.click_overview_tab_at_training();
        TabsPage.training_overview_tab();
        TabsPage.training_questionnaires_tab();
        TabsPage.training_medias_tab();
        TabsPage.training_participants_tab();
        TabsPage.training_chat_tab();
        TabsPage.training_black_board_tab();
        TabsPage.training_results_tab();
        TabsPage.training_attendance_sheet_tab();
        TabsPage.training_surveys_tab();
        TabsPage.click_see_as_admin_button();
    }

    //To verify all tabs at modules page
    verify_all_tabs_at_modules_page(modulesdata){
        modulespage.click_modules_side_menu();
        cy.wrap(modulesdata).each((data)=>{
            modulespage.open_created_module(data.module_name);
        })
        TabsPage.module_overview_tab();
        TabsPage.module_elements_tab();
        TabsPage.module_steps_tab();
        TabsPage.module_invitation_tab();
        TabsPage.module_results_tab();
        TabsPage.module_visibility_tab();
    }

    //TO verify all tabs at path page
    verify_all_tabs_at_path_page(pathdata){
        pathspage.click_path_side_menu();
        pathspage.open_created_path(pathdata);
        TabsPage.path_overview_tab();
        TabsPage.path_modules_tab();
        TabsPage.path_invitation_tab();
        TabsPage.path_results_tab();
    }

    //To verify all tabs at survey page
    verify_all_tabs_at_surveys_page(surveydata){
        survyespage.click_surveys_side_menu();
        survyespage.open_created_survey(surveydata);
        TabsPage.surveys_overview_tab();
        TabsPage.surveys_invitation_tab();
        TabsPage.surveys_results_tab();
    }

    //To verify all tabs at certificates page
    verify_all_tabs_at_certificates_page(){
        certificatespage.click_certificates_side_menu();
        TabsPage.certificates_certifications_tab();
        TabsPage.certificates_modules_tab();
    }

    //To verify all tabs at settings page
    verify_all_tabs_at_settings_page(){
        settingpage.click_setting_side_menu()
        TabsPage.settings_enterprise_tab();
        TabsPage.settings_settings_tab();
        TabsPage.settings_customization_tab();
        TabsPage.settings_administration_tab();
    }

    //To open the user view from admin account
    open_user_view(){
        TabsPage.click_user_view_side_menu();
    }

    //To open the admin view from admin account
    open_admin_view(){
        TabsPage.click_admin_view_side_menu()
    }

    //To verify all tabs at my result page of user account
    verify_all_tabs_at_users_my_results(){
        TabsPage.click_my_results_side_menu();
        TabsPage.my_results_evaluation_tab();
        TabsPage.my_results_free_tests_tab();
    }

    //To verify all tabs at my certificates page of user account
    verify_all_tabs_at_users_my_certificates(){
        TabsPage.click_my_certificates_side_menu();
        TabsPage.certificates_certifications_tab();
        TabsPage.certificates_modules_tab();
    }

    //To verify all tabs at my skills page of user account
    verify_all_tabs_at_users_my_skills(){
        TabsPage.click_my_skills_side_menu();
        TabsPage.see_activities_skills_tab_into_skills();
        TabsPage.see_activities_qualifications_tab_into_skills();
        TabsPage.see_activities_jobs_tab_into_skills();
        TabsPage.see_activities_history_tab_into_skills();
    }

    //To verify all tabs at Business Intelligence
    verify_all_tabs_at_business_intelligence(){
        TabsPage.click_business_intelligence_side_menu();
        TabsPage.business_intelligence_analytics();
        TabsPage.business_intelligence_charts();
        TabsPage.click_dashboard_at_business_intelligence();
        TabsPage.business_intelligence_overview();
        TabsPage.business_intelligence_description();
        TabsPage.business_intelligence_visibility();
        TabsPage.click_business_intelligence_side_menu();
        TabsPage.click_business_intelligence_charts();
        TabsPage.click_charts_at_business_intelligence();
        TabsPage.business_intelligence_overview();
        TabsPage.business_intelligence_description();
    }

}
export default HomeTabsAction
