import BasePage from '../support/PageObject/BasePage.js';
import QuestionnairePage from "../support/PageObject/QuestionnairePage.js";
import SurveysPage from "../support/PageObject/SurveysPage.js";
import BaseModulePage from "../support/PageObject/BaseModulePage.js";
import SettingPage from "../support/PageObject/SettingPage.js";

const basepage = new BasePage;
const questionnairepage = new QuestionnairePage()
const survyespage = new SurveysPage();
const baseModulepage = new BaseModulePage();
const settingpage = new SettingPage()

class BaseModuleAction {

    //
    createQuestionnaireWithDifferentMode(baseName, simple_surveydata, randomNumber) {
        var modes = ["public", "visibility", "form", "renewal", "consequence"]
        this.create_surveys("Base survey " + randomNumber)
        this.editSurvey("Base survey " + randomNumber, simple_surveydata)
        for (let mode in modes) {
            basepage.click_admin_base_menu()
            basepage.open_created_base(baseName)
            basepage.click_questionnaire_tab()
            basepage.click_new_questionnaire_button()
            baseModulepage.enterQuestinnaireName("Questionnaire " + modes[mode]+" "+randomNumber)
            questionnairepage.select_dynamic_type_questionnaire()
            questionnairepage.click_create_button_on_questionnaire_create_page()
            baseModulepage.enterQuestionCount("5")
            baseModulepage.saveButton()
            basepage.click_admin_base_menu()
            basepage.open_created_base(baseName)
            basepage.click_questionnaire_tab()
            if (modes[mode] === "public") {
                baseModulepage.clickVisibility("Questionnaire " + modes[mode]+" "+randomNumber)
                baseModulepage.MakeItPublic("questionnairemode" + modes[mode] + randomNumber)
            }
            if (modes[mode] === "visibility") {
                baseModulepage.clickVisibility("Questionnaire " + modes[mode]+" "+randomNumber)
                baseModulepage.MakeItVisible()
            }
            if (modes[mode] === "form") {
                baseModulepage.clickTestSettings("Questionnaire " + modes[mode]+" "+randomNumber)
                baseModulepage.MakeForm("Base survey " + randomNumber)
            }
            if (modes[mode] === "renewal") {
                baseModulepage.clickTestSettings("Questionnaire " + modes[mode]+" "+randomNumber)
                baseModulepage.MakeRenewal()
            }
            if (modes[mode] === "consequence") {
                baseModulepage.clickTestSettings("Questionnaire " + modes[mode]+" "+randomNumber)
                baseModulepage.MakeConsequence()
            }
        }
    }

    create_surveys(surveyname) {
        survyespage.click_surveys_side_menu();
        survyespage.click_new_form_button();
        baseModulepage.enter_survey_name(surveyname);
        survyespage.click_save_button_at_survey_creation_page();
    }

    editSurvey(surveyname, surveydata) {
        survyespage.click_surveys_side_menu();
        baseModulepage.openCreatedSurvey(surveyname);
        survyespage.click_edit_button();
        survyespage.create_all_fields_on_created_surveys(surveydata)
        survyespage.click_save_button_at_survey_creation_page();
    }

    createDuplicateBase(baseName) {
        basepage.click_admin_base_menu()
        baseModulepage.clickDuplicate(baseName)
    }

    verifyQuestionStatus(baseName) {
        const QuestionStatus = ["Draft", "Proposed", "Reviewed", "Inactive", "Active"]
        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedDuplicate(baseName)
        basepage.click_questions_tab()
        QuestionStatus.forEach(status => {
            baseModulepage.changeStatusAndVerifyCopied(status)
        })
    }

    verifyDuplicateBaseCreatedAsExpected(baseName,mediadata) {
        //To verify that active questions only copied to the dublicate base(Draft,Proposed,Reviewed)
        this.verifyQuestionStatus(baseName)
        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedDuplicate(baseName)
        baseModulepage.clickOverviewTab()
        let actual_questionCount
        let actual_questionnaireCount

        cy.xpath("//label[text()='Questions']/../div").then($el => {
            actual_questionCount = $el[0].innerText
            cy.readFile('cypress/fixtures/questionData.json').its('questionCount').should('eq', Number(actual_questionCount))
        })
        cy.xpath("//label[text()='Questionnaires']/../div").then($el => {
            actual_questionnaireCount = $el[0].innerText
            cy.readFile('cypress/fixtures/questionnaireData.json').its('questionnaireCount').should('eq', Number(actual_questionnaireCount))
        })

        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedDuplicate(baseName)
        basepage.click_questionnaire_tab()
        //public icon is removed only after open the Questionnaire public
        baseModulepage.clickVisibility("Questionnaire public")
        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedDuplicate(baseName)
        basepage.click_questionnaire_tab()
        baseModulepage.verifyDuplicatedQuestionnaireModes()
        this.verifyDuplicateMedia(baseName,mediadata)
    }

    openOverviewTab(baseName) {
        basepage.click_admin_base_menu()
        basepage.open_created_base(baseName)
        baseModulepage.clickOverviewTab()
    }

    getQuestionsCount() {
        baseModulepage.getQuestionCount()
    }

    getQuestionnaireCount() {
        baseModulepage.getQuestionnaireCount()
    }

    EditQuestionsToChangeStatus(baseName, question) {
        basepage.click_admin_base_menu();
        basepage.open_created_base(baseName);
        basepage.click_questions_tab();
        const QuestionStatus = ["Draft", "Proposed", "Reviewed", "Inactive"]
        let i = 1;
        QuestionStatus.forEach(status => {
            baseModulepage.clickEditIconOfQuestion(i)
            baseModulepage.chooseStatus(status)
            baseModulepage.clickSaveAndQuit()
            i++;
        })
    }

    TransferBase(baseName, randomNumber) {
        basepage.click_admin_base_menu();
        basepage.open_created_base(baseName);
        baseModulepage.ClickTransferButton();
        baseModulepage.EnterExportFilName(randomNumber)
        baseModulepage.ClickBuildTransferButton()

    }

    CopyLinkOfTranferBase(randomNumber) {
        settingpage.click_setting_side_menu()
        settingpage.click_administration_tab()
        settingpage.ClickDefferedJobs()
        baseModulepage.getTransferLink()
    }

    transferByLinkAndVerify(baseName) {
        baseModulepage.visitLinkToTransfer()
        basepage.click_admin_base_menu()
        baseModulepage.verifyBaseTransfered(baseName)
    }

    verifyQuestionStatusOfTransferedBase(baseName) {
        const QuestionStatus = ["Draft", "Proposed", "Reviewed", "Inactive", "Active"]
        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedTransferBase(baseName)
        basepage.click_questions_tab()
        QuestionStatus.forEach(status => {
            baseModulepage.changeStatusAndVerifyCopiedInTransfer(status)
        })
    }

    verifyTransferBaseCreatedAsExpected(baseName,mediadata) {
        //To verify that active questions only copied to the Tranfered base(Draft,Proposed,Reviewed)
        this.verifyQuestionStatusOfTransferedBase(baseName)
        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedTransferBase(baseName)
        baseModulepage.clickOverviewTab()
        let actual_questionCount
        let actual_questionnaireCount

        cy.xpath("//label[text()='Questions']/../div").then($el => {
            actual_questionCount = $el[0].innerText
            cy.readFile('cypress/fixtures/questionData.json').its('questionCount').should('eq', Number(actual_questionCount))
        })
        cy.xpath("//label[text()='Questionnaires']/../div").then($el => {
            actual_questionnaireCount = $el[0].innerText
            cy.readFile('cypress/fixtures/questionnaireData.json').its('questionnaireCount').should('eq', Number(actual_questionnaireCount))
        })

        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedTransferBase(baseName)
        basepage.click_questionnaire_tab()
        //public icon is removed only after open the Questionnaire public
        baseModulepage.clickVisibility("Questionnaire public")
        basepage.click_admin_base_menu()
        baseModulepage.clickCreatedTransferBase(baseName)
        basepage.click_questionnaire_tab()
        baseModulepage.verifyTransferedQuestionnaireModes()
        this.verifyTransferMedia(baseName,mediadata)
    }


    verifyDuplicateMedia(baseName,mediadata) {
        basepage.click_admin_base_menu();
        baseModulepage.clickCreatedDuplicate(baseName);
        basepage.click_media_tab_on_base()
        baseModulepage.verifyMediaExist(mediadata)
    }

    verifyTransferMedia(baseName, mediadata) {
        basepage.click_admin_base_menu();
        baseModulepage.clickCreatedTransferBase(baseName);
        basepage.click_media_tab_on_base()
        baseModulepage.verifyMediaExist(mediadata)
    }
}

export default BaseModuleAction
