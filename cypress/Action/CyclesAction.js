import CyclesPage from "../support/PageObject/CyclesPage";
import AdminDashboardPage from "../support/PageObject/AdminDashboardPage.js";
import HomePage from "../support/PageObject/HomePage";


const cyclespage = new CyclesPage();
const admindashboardpage = new AdminDashboardPage();
const homepage = new HomePage()

class CyclesAction
{
    //To create cycles
    create_cycles(cycledata)
    {
        cyclespage.click_cycles_side_menu();
        cyclespage.click_new_cycle_button();
        cyclespage.enter_cycle_name(cycledata);
        cyclespage.enter_description(cycledata);
        cyclespage.click_cycle_save_button();
        cyclespage.verify_created_cycle(cycledata);
    }

    //To create steps into already created cycles
    create_steps_into_created_cycles(basedatas,cycle){
        cyclespage.click_cycles_side_menu();
        cyclespage.open_created_cycles(cycle);
        cyclespage.click_steps_tab();
        cyclespage.filter_created_base_on_steps_page(basedatas);
        let steps = cycle.cycle_step
        cy.wrap(steps).each((stepdata, index)=> {
            let type = ["questionnaire"]
            let form = ["form"]
            cy.log('my type is-----', stepdata.type)
        if(stepdata.type === "questionnaire") {
                cyclespage.select_questionnaire_to_cycle(stepdata);
                cyclespage.set_delay_as_persent_on_questionnaire(basedatas, stepdata)
                cyclespage.uncheck_all_days(basedatas, stepdata)
                cyclespage.set_day_as_persent(basedatas, stepdata)
        }
        else if(stepdata.type === "form") {
                cyclespage.filter_all_on_steps_page();
                cyclespage.select_form_on_cycle(stepdata)
                cyclespage.set_delay_as_persent_on_form(stepdata)
        }
        })
        cyclespage.click_save_button_cycles_step_page();
    }

    invite_cycle_with_3_messages_to_contact_user(cycledata, contact, randno, mail_sub){
        cyclespage.click_cycles_side_menu();
        cyclespage.open_created_cycles(cycledata);
        cyclespage.click_invitation_tab_on_cycle_invitation_page()
        cyclespage.enable_yes_on_cycle_invitation_page()
        cyclespage.enter_subject_on_cycle_invitation_page(mail_sub, randno)
        cyclespage.enter_message_on_cycle_invitation_page(mail_sub)
        cyclespage.click_emails_and_mobile_button()
        cyclespage.enter_contact_user_mail_id(contact)
        cyclespage.click_save_button_on_emails_and_mobile_window()
        cyclespage.click_invite_button_on_cycle_invitation_page()
        homepage.log_out_admin()
    }

    invite_cycle_with_3_messages_to_active_user(cycledata, usersdatas, randno, mail_sub){
        cyclespage.click_cycles_side_menu();
        cyclespage.open_created_cycles(cycledata);
        cyclespage.click_invitation_tab_on_cycle_invitation_page()
        cyclespage.enable_yes_on_cycle_invitation_page()
        cyclespage.enter_subject_on_cycle_invitation_page(mail_sub, randno)
        cyclespage.enter_message_on_cycle_invitation_page(mail_sub)
        cyclespage.click_select_user_button_on_cycle_invitation_page()
        cyclespage.select_active_user(usersdatas)
        cyclespage.click_validate_button_on_select_user_popup()
        cyclespage.click_invite_button_on_cycle_invitation_page()
        homepage.log_out_admin()
    }

    verify_contact_user_cycle_result(cycledata, contact){
        cyclespage.click_cycles_side_menu();
        cyclespage.open_created_cycles(cycledata);
        cyclespage.click_users_tab_on_cycle()
        cyclespage.filter_all_status_on_cycle_user_page()
        cyclespage.verify_contact_user_played_cycle_test(contact)
    }

    verify_delete_ongoing_cycle_on_user_page(cycledata, user){
        cyclespage.click_cycles_side_menu();
        cyclespage.open_created_cycles(cycledata);
        cyclespage.click_users_tab_on_cycle()
        cyclespage.filter_ongoing_status_on_cycle_user_page()
        cyclespage.click_checkbox_on_ongoing_cycle()
        cyclespage.click_delete_button_on_cycle_user_page()
        cyclespage.click_delete_button_on_confirmation_cycle_user_page()
        cyclespage.filter_canceled_status_on_cycle_user_page()
        cyclespage.verify_deleted_ongoing_cycle_is_exist_on_canceled_status(user)
        homepage.log_out_admin()
    }

    verify_active_user_cycle_result(cycledata, user){
        cyclespage.click_cycles_side_menu();
        cyclespage.open_created_cycles(cycledata);
        cyclespage.click_users_tab_on_cycle()
        cyclespage.verify_active_user_played_cycle_result(user)
    }



}
export default CyclesAction